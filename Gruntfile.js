module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            admin: {
                src: [
                    'app/app/static/admin/js/jquery.min.js',
                    'app/app/static/admin/js/bootstrap.min.js',
                    'app/app/static/admin/js/bootstrap-markdown.js',
                    'app/app/static/admin/js/bootstrap-markdown.ru.js',
                    'app/app/static/admin/js/markdown.js',
                    'app/app/static/admin/js/to-markdown.js',
                    'app/app/static/admin/js/bootstrap-datetimepicker.js',
                    'app/app/static/admin/js/locales/bootstrap-datetimepicker.ru.js',
                    'app/app/static/admin/js/admin.js'
                ],
                dest: 'app/static/admin/js/admin.js'
            },
            public: {
                src: [
                    'app/app/static/public/js/jquery.min.js',
                    'app/app/static/public/js/bootstrap.min.js',
                    'app/app/static/public/js/owl.carousel.min.js',
                    'app/app/static/public/js/jquery.mousewheel-3.0.6.pack.js',
                    'app/app/static/public/js/jquery.fancybox.js',
                    'app/app/static/public/js/shop.js'
                ],
                dest: 'app/static/public/js/public.js'
            }

        },
        uglify: {
            options: {
                mangle: false
            },
            target: {
                files: {
                    'app/static/admin/js/admin.min.js': ['app/static/admin/js/admin.js'],
                    'app/static/public/js/public.min.js': ['app/static/public/js/public.js']
                }
            }
        },
        clean: {
            js: ['app/static/admin/js/admin.js', 'app/static/public/js/public.js']
        },
        cssmin: {
            admin: {
                files: {
                    'app/static/admin/css/admin.min.css': [
                        'app/app/static/admin/css/bootstrap.min.css',
                        'app/app/static/admin/css/select2.css',
                        'app/app/static/admin/css/select2-bootstrap.css',
                        'app/app/static/admin/css/bootstrap-markdown-editor.css',
                        'app/app/static/admin/css/datetimepicker.css',
                        'app/app/static/admin/css/bootstrap-datetimepicker.css',
                        'app/app/static/admin/css/style.css'
                    ]
                }
            },
            public: {
                files: {
                    'app/static/public/css/public.min.css': [
                        'app/app/static/public/css/font-ubuntu.css',
                        'app/app/static/public/css/font-pacifico.css',
                        'app/app/static/public/css/font-awesome.css',
                        'app/app/static/public/css/bootstrap.min.css',
                        'app/app/static/public/css/style.css',
                        'app/app/static/public/css/owl.carousel.css',
                        'app/app/static/public/css/owl.transitions.css',
                        'app/app/static/public/css/jquery.fancybox.css'
                    ]
                }
            }
        },
        sync: {
            admin: {
                files: [
                    {cwd: 'app/app/static/admin/fonts/', src: '**', dest: 'app/static/admin/fonts/'},
                    {cwd: 'app/app/static/admin/img/', src: '**', dest: 'app/static/admin/img/'}
                ]
            },
            main: {
                files: [
                    {cwd: 'app/app/static/public/fonts/', src: '**', dest: 'app/static/public/fonts/'},
                    {cwd: 'app/app/static/public/images/', src: '**', dest: 'app/static/public/images/'}
                ],
            }
        },
        uncss: {
            dist: {
                files: {
                    'app/static/admin/css/admin.min.css': [
                        'app/templates/*.html',
                        'app/templates/admin/*.html',
                        'app/templates/admin/**/*.html'
                    ]
                }
            }
        },
        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 5,
                success: true,
                duration: 0
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-sync');
    grunt.loadNpmTasks('grunt-notify');
    grunt.registerTask('default', [
        'concat',
        'uglify',
        'clean',
        'cssmin',
        'sync',
    ]);
    grunt.task.run('notify_hooks');
};