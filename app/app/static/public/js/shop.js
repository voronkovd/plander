jQuery(document).ready(function ($) {
    "use strict";
    // Карусель
    $(function () {
        var owl = $("#product-carousel");
        owl.owlCarousel({
            items: 4,
            autoPlay: 5000,
            goToFirstSpeed: 3000,
            pagination: false
        });

        $(".next").click(function () {
            owl.trigger('owl.next');
        });
        $(".prev").click(function () {
            owl.trigger('owl.prev');
        });
    });
    $(function () {

        var sync1 = $("#sync1");
        var sync2 = $("#sync2");

        sync1.owlCarousel({
            singleItem: true,
            slideSpeed: 1000,
            navigation: true,
            autoPlay: 5000,
            navigationText: ["<i class='sprv'></i>", "<i class='snxt'></i>"],
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200
        });

        sync2.owlCarousel({
            items: 5,
            pagination: false,
            responsiveRefreshRate: 100,
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        function syncPosition(el) {
            var current = this.currentItem;
            sync2
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced");
            if (sync2.data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        sync2.on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }

    });
    $(function () {
        $("#reg").click(function () {
            $('.regwrap').toggle('fast', function () {
            });
        });
    });
    $(function () {
        $('body').on('click', '#popcart', function () {
            $('.popcart').toggle('fast', function () {
            });
        });
    });
    $(function () {
        $('.fancybox').fancybox();
    });
    $(function () {

        $('.change_product').on('change', function () {
            var url = $('option:selected', this).attr('data-url');
            window.location.href = url;
        });
    });
    // Корзина
    $(function () {
        var basket = $('.add_basket');
        var qty = $('#qty');
        var delete_basket = $('.delete-from-basket');
        var count_basket = $('.basket-count');
        var all_sum = $('.bigprice');
        var to_order = $('#to-order');
        qty.on('change', function (e) {
            var count = $(this).val();
            basket.attr('data-count', count);
        });
        delete_basket.on('click', function () {
            if (confirm('Удалить?')) {
                $.ajax({
                    url: $(this).attr('href'),
                    complete: function () {
                        window.location.reload();
                    }
                });
            }
            return false;
        });

        to_order.on('click', function () {
            if (confirm('Оформить заказ?')) {
                return true;
            }
            return false;
        });
        count_basket.on('change', function (e) {
            var count = $(this).val();
            var id = $(this).attr('data-id');
            $.ajax({
                url: '/orders/basket/edit/?id=' + id + '&count=' + count,
                complete: function (data) {
                    data = data.responseJSON;
                    all_sum.html(data.all_sum);
                }
            });
        });

        basket.on('click', function () {
            var store = $(this).attr('data-id');
            var hash = $(this).attr('data-hash');
            var count = $(this).attr('data-count');
            $.ajax({
                url: '/orders/to/basket/?store=' + store + '&hash=' + hash + '&count=' + count,
                complete: function (data) {
                    if (data.status == 403) {
                        window.location.href = '/accounts/login'
                    }
                    if (data.status == 404) {
                        alert('Товар не найден');
                    }
                    if (data.status == 200) {
                        $('.machart').html(data.responseText);
                    }
                }
            });
            return false;
        });
    });
});