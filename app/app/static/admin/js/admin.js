$(document).ready(function () {
    var description_input = $('#id_markdown_editor');
    var group_select = $('#id_base_group');
    var brand_select = $('#id_base_brand');
    var good_select = $('#id_store_good');
    var store_brand_select = $('#id_store_brand');
    var description_options = {
        autofocus: false,
        hideable: false,
        savable: false,
        width: "100%",
        height: 200,
        resize: "none",
        iconlibrary: "glyph",
        footer: "",
        language: "ru",
        fullscreen: {enable: false},
        hiddenButtons: ['Image']
    };

    description_input.markdown(description_options);

    brand_select.on('change', function (e) {
        var this_value = $(this).val();
        if (this_value != '') {
            $.ajax({
                method: 'GET',
                dataType: 'html',
                url: '/admin/products/groups/brand/' + this_value,
                complete: function (data) {
                    if (data.status == 200) {
                        group_select.html(data.responseText);
                    } else {
                        group_select.html('<option>---------</option>');
                    }
                }
            });
        }
    });

    store_brand_select.on('change', function (e) {
        var this_value = $(this).val();
        var is_color = $('#id_color');
        var is_size = $('#id_size');
        var is_volume = $('#id_volume');
        var is_weight = $('#id_weight');
        is_color.attr('disabled', 'disabled');
        is_size.attr('disabled', 'disabled');
        is_volume.attr('disabled', 'disabled');
        is_weight.attr('disabled', 'disabled');
        if (this_value != '') {
            $.ajax({
                method: 'GET',
                dataType: 'html',
                url: '/admin/products/groups/bases/' + this_value,
                complete: function (data) {
                    if (data.status == 200) {
                        good_select.html(data.responseText);
                    } else {
                        good_select.html('<option>---------</option>');
                    }
                }
            });
        }
    });

    good_select.on('change', function (e) {
        var this_value = parseInt($(this).val());
        var is_color = $('#id_color');
        var is_size = $('#id_size');
        var is_volume = $('#id_volume');
        var is_weight = $('#id_weight');
        if (this_value != '' && this_value != null && !isNaN(this_value)) {
            $.ajax({
                method: 'GET',
                dataType: 'json',
                url: '/admin/products/bases/ajax/' + this_value,
                complete: function (data) {
                    if (data.status == 200) {
                        data = data.responseJSON;
                        if (data.object.is_color == true) {
                            is_color.removeAttr('disabled');
                        }
                        if (data.object.is_size == true) {
                            is_size.removeAttr('disabled');
                        }
                        if (data.object.is_volume == true) {
                            is_volume.removeAttr('disabled');
                        }
                        if (data.object.is_weight == true) {
                            is_weight.removeAttr('disabled');
                        }
                    } else {
                        is_color.attr('disabled', 'disabled');
                        is_size.attr('disabled', 'disabled');
                        is_volume.attr('disabled', 'disabled');
                        is_weight.attr('disabled', 'disabled');
                    }
                }
            });
        }
    });
    good_select.trigger('change');
});