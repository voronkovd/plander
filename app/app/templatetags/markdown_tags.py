# -*- coding: utf-8 -*-
import markdown

from django import template

register = template.Library()


@register.filter(name="markdown_to_text")
def markdown_to_text(text):
    return markdown.markdown(text, safe_mode='escape')
