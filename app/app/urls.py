# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns
from django.conf.urls import include
from django.conf import settings
from public.views import user
from users.backends import RegistrationView
from users.forms import CustomRegistrationForm

urlpatterns = patterns('',
                       (r'^$', include('public.urls')),
                       (r'^pages/', include('public.page_urls')),
                       (r'^products/', include('public.product_urls')),
                       (r'^orders/', include('public.order_urls')),
                       url(r'^accounts/profile/$',
                           user.profile_view, name='public_user_profile'),
                       (r'^accounts/register/$',
                        RegistrationView.as_view(
                            form_class=CustomRegistrationForm)),
                       (r'^accounts/',
                        include('registration.backends.default.urls')),
                       (r'^admin/', include('admin.urls')),
                       url(r'^select2/', include('django_select2.urls')),
                       (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                        dict(document_root=settings.MEDIA_ROOT)),
                       (r'^static/(?P<path>.*)$',
                        'django.views.static.serve',
                        dict(document_root=settings.STATIC_ROOT))
                       )
