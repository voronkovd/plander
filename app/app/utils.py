# -*- coding: utf-8 -*-
import hashlib
import uuid

from django.conf import settings


def upload_to(instance, filename):
    """
    Генерируем папку и наименования файла для загрузки
    :param instance:
    :param filename:
    :return:
    """
    h = str(uuid.uuid4().hex)
    folder = str(instance.__class__.__name__).lower()
    new_filename = "%s.%s" % (h, filename.split('.')[-1].lower())
    return '{}/{}'.format(folder, new_filename)


def generate_hash(value):
    md = hashlib.md5()
    md.update(settings.SECRET_KEY + str(value))
    return md.hexdigest()


def check_hash(value, value_hash):
    if generate_hash(value) == value_hash:
        return True
    return False
