# -*- coding: utf-8 -*-
import os
import ConfigParser
from easy_thumbnails.conf import Settings as thumbnail_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
APP_DIR = os.path.abspath(os.path.join(BASE_DIR, '..'))
my_config = ConfigParser.ConfigParser()
my_config.readfp(open(BASE_DIR + '/app/settings.ini'))

SECRET_KEY = my_config.get('main', 'secret_key')
DEBUG = my_config.getboolean('main', 'debug')
TEMPLATE_DEBUG = DEBUG
ADMINS = (
    (my_config.get('main', 'admin_name'),
     my_config.get('main', 'admin_email')),
)
ABOUT = my_config.get('main', 'about')
ALLOWED_HOSTS = [my_config.get('main', 'allowed_hosts')]
ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
LANGUAGE_CODE = my_config.get('main', 'language')
TIME_ZONE = my_config.get('main', 'timezone')
USE_I18N = True
USE_L10N = True
USE_TZ = my_config.getboolean('main', 'use_tz')

# django
INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
)

# utils
INSTALLED_APPS += (
    'easy_thumbnails',
    'image_cropping',
    'pagination',
    'bootstrapform',
    'django_select2',
    'django_cache_manager',
    'django_filters',
    'registration',
    'datetimewidget',
)

# apps
INSTALLED_APPS += (
    'app',
    'orders',
    'products',
    'users',
    'admin',
    'public',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'pagination.middleware.PaginationMiddleware',
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': my_config.get('mysql', 'name'),
        'USER': my_config.get('mysql', 'user'),
        'PASSWORD': my_config.get('mysql', 'password'),
        'HOST': my_config.get('mysql', 'host'),
    }
}

SESSION_ENGINE = 'redis_sessions.session'

SESSION_REDIS_HOST = '127.0.0.1'
SESSION_REDIS_PORT = 6379
SESSION_REDIS_DB = 0
SESSION_REDIS_PREFIX = 'session'

CACHES = {
    'default': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': ['127.0.0.1:6379'],
        'OPTIONS': {
            'DB': 1,
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'MAX_CONNECTIONS': 1000,
            'PICKLE_VERSION': -1
        }
    },
    'django_cache_manager.cache_backend': {
        'BACKEND': 'redis_cache.RedisCache',
        'LOCATION': ['127.0.0.1:6379'],
        'OPTIONS': {
            'DB': 1,
            'PARSER_CLASS': 'redis.connection.HiredisParser',
            'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
            'CONNECTION_POOL_CLASS_KWARGS': {
                'max_connections': 50,
                'timeout': 20,
            },
            'MAX_CONNECTIONS': 1000,
            'PICKLE_VERSION': -1
        }
    }
}

BACKUP_DIR = APP_DIR + '/backups/'

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    'app.context_processors.debug',
)

TEMPLATE_DIRS = (BASE_DIR + '/templates/',)

STATIC_ROOT = BASE_DIR + '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = APP_DIR + '/media/'
STATICFILES_DIRS = ()
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_PASSWORD = my_config.get('email', 'password')
EMAIL_HOST_USER = my_config.get('email', 'user')
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
SERVER_EMAIL = DEFAULT_FROM_EMAIL
EMAIL_PORT = 465
EMAIL_USE_SSL = True

ERROR_LOG_FILE = APP_DIR + '/logs/' + 'error.log'
INFO_LOG_FILE = APP_DIR + '/logs/' + 'info.log'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'formatters': {
        'main_formatter': {
            'format': '%(levelname)s:%(name)s: %(message)s '
                      '(%(asctime)s; %(filename)s:%(lineno)d)',
            'datefmt': "%Y-%m-%d %H:%M:%S",
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'main_formatter',
        },
        'production_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': INFO_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_false'],
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': ERROR_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 7,
            'formatter': 'main_formatter',
            'filters': ['require_debug_true'],
        },
        'null': {
            "class": 'django.utils.log.NullHandler',
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'console'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['null', ],
        },
        'py.warnings': {
            'handlers': ['null', ],
        },
        '': {
            'handlers': ['console', 'production_file', 'debug_file'],
            'level': "DEBUG",
        },
    }
}

MARKDOWN_CONFIG = {
    'width': "100%",
    'height': 200,
    'locale': 'ru',
    'boostrap_cdn': False
}

ACCOUNT_ACTIVATION_DAYS = 2
LOGIN_REDIRECT_URL = '/'
REGISTRATION_OPEN = True
AUTH_USER_EMAIL_UNIQUE = True
REGISTRATION_AUTO_LOGIN = True
AUTH_USER_MODEL = "users.User"

IMAGE_FULL_WIDTH = 334
IMAGE_FULL_HEIGHT = 222

IMAGE_HELP_TEXT = "Минимальный размер изображения %ix%i" % (
    IMAGE_FULL_WIDTH, IMAGE_FULL_HEIGHT)

THUMBNAIL_PROCESSORS = (
                           'image_cropping.thumbnail_processors.crop_corners',
                       ) + thumbnail_settings.THUMBNAIL_PROCESSORS
IMAGE_CROPPING_SIZE_WARNING = True

SELECT2_JS = 'admin/js/select2.min.js'
