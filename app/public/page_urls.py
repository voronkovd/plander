# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns
from public.views import page

urlpatterns = patterns('',
                       url(r'^delivery/$',
                           page.delivery_view, name='public_page_delivery'),
                       url(r'^contact/$',
                           page.contact_view, name='public_page_contact'),
                       )
