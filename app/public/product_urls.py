# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns

from public.views import product

urlpatterns = patterns('',
                       url(r'^categories/(?P<slug>[-\w]+)/$',
                           product.index_view, name='public_category_list'),
                       url(r'^search/$',
                           product.search_view, name='public_search_list'),
                       url(r'^(?P<c>[-\w]+)/(?P<slug>[-\w]+)/$',
                           product.detail_view, name='public_product_detail'),
                       )
