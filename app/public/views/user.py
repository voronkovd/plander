# -*- coding: utf-8 -*-
from django.views import generic

from public.forms import UserForm
from users.views.permission import LoginRequiredMixin


class Profile(LoginRequiredMixin, generic.FormView):
    form_class = UserForm
    success_url = '/accounts/profile/'
    template_name = 'public/users/profile.html'

    def get_form_kwargs(self):
        kwargs = super(Profile, self).get_form_kwargs()
        kwargs['instance'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(Profile, self).form_valid(form)


profile_view = Profile.as_view()
