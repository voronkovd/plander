# -*- coding: utf-8 -*-
from django.db.models import Q
from django.http import Http404
from django.views import generic
from django.shortcuts import get_object_or_404

from products.models import GoodBase, GoodGroup, GoodStore, GoodTag


class Detail(generic.DetailView):
    model = GoodStore
    queryset = GoodStore.objects.filter(is_active=True).select_related(
        'good', 'color', 'size', 'volume', 'weight')
    template_name = 'public/products/detail.html'


class Index(generic.ListView):
    group = None
    model = GoodStore
    paginate_by = 9
    template_name = 'public/products/index.html'

    def get_queryset(self):
        self.group = get_object_or_404(GoodGroup, slug=self.kwargs['slug'])
        return GoodStore.objects.filter(good__group__slug=self.kwargs['slug'],
                                        is_active=True).select_related('good')

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['group'] = self.group
        return context


class Search(generic.ListView):
    model = GoodStore
    paginate_by = 9
    template_name = 'public/products/search.html'

    def get_queryset(self):
        q = self.request.GET.get('q')
        try:
            tags = GoodTag.objects.filter(name__in=q.split())
            kwargs = reduce(lambda x, y: x & y, [Q(tag=tag)
                                                 for tag in tags])
            return GoodStore.objects.filter(kwargs, is_active=True
                                            ).select_related('good')
        except:
            raise Http404


detail_view = Detail.as_view()
index_view = Index.as_view()
search_view = Search.as_view()
