# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.views import generic

from orders.models import Basket, Order, OrderStatus
from users.views.permission import LoginRequiredMixin


class Create(LoginRequiredMixin, generic.TemplateView):
    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        try:
            Basket.objects.filter(user=self.request.user)
            order = Order.objects.create(user=self.request.user)
            url = reverse('public_order_detail', kwargs={'pk': order.pk})
            context['url'] = url
        except Basket.DoesNotExist:
            raise Http404
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return HttpResponseRedirect(context['url'])


class Detail(LoginRequiredMixin, generic.DetailView):
    model = Order
    template_name = 'public/orders/detail.html'

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user)


class Index(LoginRequiredMixin, generic.ListView):
    model = Order
    paginate_by = 20
    template_name = 'public/orders/index.html'

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user).order_by('-id')


class Update(LoginRequiredMixin, generic.DetailView):
    model = Order
    permission_required = 'products.can_change_orders'

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user)

    def get(self, request, *args, **kwargs):
        order = self.get_object()
        url = reverse('public_order_detail', kwargs={'pk': order.pk})
        status = int(request.GET.get('status'))
        if status in Order.get_all_statuses():
            order.status = status
            order.save()
            OrderStatus.objects.create(
                user=request.user, order=order, status=status)
        return HttpResponseRedirect(url)


create_view = Create.as_view()
detail_view = Detail.as_view()
index_view = Index.as_view()
update_view = Update.as_view()
