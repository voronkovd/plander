# -*- coding: utf-8 -*-
from django.views import generic

from products.models import GoodStore


class Index(generic.ListView):
    model = GoodStore
    paginate_by = 6
    queryset = GoodStore.objects.filter(
        is_active=True).select_related('good').order_by('-id')
    template_name = 'public/index.html'


class Delivery(generic.TemplateView):
    template_name = 'public/pages/delivery.html'


class Contact(generic.TemplateView):
    template_name = 'public/pages/contact.html'


index_view = Index.as_view()
contact_view = Contact.as_view()
delivery_view = Delivery.as_view()
