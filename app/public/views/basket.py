# -*- coding: utf-8 -*-
import ujson

from django.contrib.humanize.templatetags.humanize import intcomma
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponse
from django.views import generic

from app.utils import check_hash
from orders.models import Basket, Order, OrderStore
from products.models import GoodStore
from users.views.permission import LoginRequiredMixin


class Create(generic.TemplateView):
    template_name = 'public/orders/_basket.html'

    def get_context_data(self, **kwargs):
        context = super(Create, self).get_context_data(**kwargs)
        # Если поьзователь не авторизован, то 403
        if not self.request.user.is_authenticated():
            raise PermissionDenied
        store_id = self.request.GET.get('store')
        hash_id = self.request.GET.get('hash')
        count = self.request.GET.get('count')
        # Если не хватает пеменных, то 404
        if not store_id or not hash_id or not count:
            raise Http404
        if not check_hash(store_id, hash_id):
            raise Http404
        try:
            store = GoodStore.objects.get(pk=store_id)
            basket = Basket.objects.get(user=self.request.user, store=store)
            basket.count += int(count)
            basket.save()
        except GoodStore.DoesNotExist:
            # если товар не найден, то 404
            raise Http404
        except Basket.DoesNotExist:
            # Если товара нет в корзине, то добавляем
            Basket.objects.create(
                user=self.request.user, store=store, count=count,
                price=store.price, price_real=store.price_real)
        all_sum = 0
        basket = Basket.objects.filter(
            user=self.request.user).all().order_by('-id')
        if basket:
            for good in basket:
                all_sum += good.count * good.price
        context['all_sum'] = int(all_sum)
        context['basket'] = basket
        return context


class Delete(LoginRequiredMixin, generic.DeleteView):
    model = Basket

    def get_queryset(self):
        return Basket.objects.filter(user=self.request.user)

    def get(self, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return HttpResponse(status=204)


class Index(LoginRequiredMixin, generic.ListView):
    model = Basket
    paginate_by = 100
    template_name = 'public/orders/basket.html'

    def get_queryset(self):
        return Basket.objects.filter(
            user=self.request.user
        ).select_related('store').order_by('price', 'id')

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['all_sum'] = 0
        for good in self.object_list:
            context['all_sum'] += good.count * good.price
        return context


class Update(LoginRequiredMixin, generic.TemplateView):
    def get_context_data(self, **kwargs):
        context = super(Update, self).get_context_data(**kwargs)
        basket_id = self.request.GET.get('id')
        count = self.request.GET.get('count')
        if not basket_id or not count:
            raise Http404
        try:
            basket = Basket.objects.get(pk=basket_id, user=self.request.user)
            basket.count = count
            basket.save()
        except Basket.DoesNotExist:
            raise Http404
        all_sum = 0
        basket = Basket.objects.filter(
            user=self.request.user).all().order_by('-id')
        if basket:
            for good in basket:
                all_sum += good.count * good.price
        context['all_sum'] = u"%s руб." % intcomma(int(all_sum))
        return context

    def render_to_response(self, context, **response_kwargs):
        context = self.convert_context_to_json(context)
        return HttpResponse(context, content_type='application/json')

    def convert_context_to_json(self, context):
        return ujson.dumps(context)


create_view = Create.as_view()
delete_view = Delete.as_view()
index_view = Index.as_view()
update_view = Update.as_view()
