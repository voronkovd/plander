# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

from public.views import order, basket

urlpatterns = patterns('',
                       url(r'^$',
                           order.index_view, name='public_order_list'),
                       url(r'^view/(?P<pk>[\w-]+)/$',
                           order.detail_view, name='public_order_detail'),
                       url(r'^update/(?P<pk>[\w-]+)/$',
                           order.update_view, name='public_order_update'),
                       url(r'^basket/to/order$',
                           order.create_view, name='public_order_to'),

                       url(r'^to/basket/$',
                           basket.create_view, name='public_order_to_basket'),
                       url(r'^basket/$',
                           basket.index_view, name='public_order_basket'),
                       url(r'^basket/delete/(?P<pk>[\w-]+)/$',
                           basket.delete_view, name='public_basket_delete'),
                       url(r'^basket/edit/$',
                           basket.update_view, name='public_basket_edit'),
                       )
