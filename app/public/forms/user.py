# -*- coding: utf-8 -*-
from django import forms

from registration.users import UserModel

User = UserModel()


class UserForm(forms.ModelForm):
    last_name = forms.CharField(required=True, min_length=2, max_length=64,
                                label=u'Фамилия')
    first_name = forms.CharField(required=True, min_length=2, max_length=64,
                                 label=u'Имя')

    class Meta:
        model = User
        fields = ['last_name', 'first_name']
