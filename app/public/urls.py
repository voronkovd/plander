# -*- coding: utf-8 -*-
from django.conf.urls import url, patterns

from public.views import page

urlpatterns = patterns('',
                       url(r'^$', page.index_view, name='public_index')
                       )
