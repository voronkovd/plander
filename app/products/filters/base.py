# -*- coding: utf-8 -*-
from django.forms.widgets import TextInput
import django_filters

from products.models import GoodBase

BOOLEAN_VALUES = ((1, u'Да'), (0, u'Нет'))


class BaseFilter(django_filters.FilterSet):
    """
    Фильтр базовых товаров, поиск по наименованиям
    """
    name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Наименование'}))
    is_color = django_filters.ChoiceFilter(
        choices=((None, u'Цвет'),)+BOOLEAN_VALUES)
    is_size = django_filters.ChoiceFilter(
        choices=((None, u'Размер'),) + BOOLEAN_VALUES)
    is_volume = django_filters.ChoiceFilter(
        choices=((None, u'Объем'),) + BOOLEAN_VALUES)
    is_weight = django_filters.ChoiceFilter(
        choices=((None, u'Вес'),) + BOOLEAN_VALUES)

    class Meta:
        model = GoodBase
        fields = ['brand', 'group', 'name', 'is_color', 'is_size',
                  'is_volume', 'is_weight']
