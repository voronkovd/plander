# -*- coding: utf-8 -*-
from django.forms.widgets import TextInput
import django_filters

from products.models import GoodGroup


class GroupFilter(django_filters.FilterSet):
    """
    Фильтр категорий товаров по наименованию
    """
    name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Наименование'}))
    parent = django_filters.ModelChoiceFilter(
        queryset=GoodGroup.objects.filter(parent__isnull=True).all())

    class Meta:
        model = GoodGroup
        fields = ['parent', 'name']
