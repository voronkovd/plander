# -*- coding: utf-8 -*-
from django.forms.widgets import TextInput
import django_filters

from products.models import GoodWeight


class WeightFilter(django_filters.FilterSet):
    """
    Фильтр по вариантам веса по наименованию
    """
    name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Наименование'}))

    class Meta:
        model = GoodWeight
        fields = ['name']
