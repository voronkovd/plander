# -*- coding: utf-8 -*-
from django.forms.widgets import TextInput
import django_filters

from products.models import GoodBrand


class BrandFilter(django_filters.FilterSet):
    """
    Фильтр брендов по наименованию
    """
    name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Наименование'}))

    class Meta:
        model = GoodBrand
        fields = ['name']
