# -*- coding: utf-8 -*-
from django.forms.widgets import TextInput
import django_filters

from products.models import GoodTag


class TagFilter(django_filters.FilterSet):
    """
    Фильтр тэгов по наименованию
    """
    name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Наименование'}))

    class Meta:
        model = GoodTag
        fields = ['name']
