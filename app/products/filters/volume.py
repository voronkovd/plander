# -*- coding: utf-8 -*-
from django.forms.widgets import TextInput
import django_filters

from products.models import GoodVolume


class VolumeFilter(django_filters.FilterSet):
    """
    Фильтр по вариантам объема по наименованию
    """
    name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Наименование'}))

    class Meta:
        model = GoodVolume
        fields = ['name']
