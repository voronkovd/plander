# -*- coding: utf-8 -*-
from django.forms.widgets import TextInput
import django_filters

from products.models import GoodStore

BOOLEAN_VALUES = ((1, u'Да'), (0, u'Нет'))


class StoreFilter(django_filters.FilterSet):
    """
    Фильтр по товарам, по наименованию базового товара
    """
    good__name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Базовый товар'})
    )
    is_available = django_filters.ChoiceFilter(
        choices=((None, u'Продавать'),) + BOOLEAN_VALUES)
    is_active = django_filters.ChoiceFilter(
        choices=((None, u'Отображать'),) + BOOLEAN_VALUES)

    class Meta:
        model = GoodStore
        fields = ['good__brand', 'good__name', 'color', 'size', 'volume', 'weight',
                  'is_available', 'is_active']
