# -*- coding: utf-8 -*-
from django import forms
from django_select2.forms import ModelSelect2Widget

from products.models import GoodGroup


class GroupForm(forms.ModelForm):
    """
    Форма группы товаров
    """
    parent = forms.ModelChoiceField(
        queryset=GoodGroup.objects.filter(parent__isnull=True).all(),
        widget=ModelSelect2Widget(search_fields=['name__icontains']),
        label=u'Группа товаров', required=False)

    class Meta:
        model = GoodGroup
        fields = ['parent', 'name']
