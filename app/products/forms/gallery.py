# -*- coding: utf-8 -*-
from django import forms

from products.forms.store import ImageAbstract
from products.models import GoodGallery


class GalleryForm(ImageAbstract):
    """
    Форма галереи изображений
    """
    class Meta:
        model = GoodGallery
        fields = ['store', 'image']
        widgets = {'store': forms.HiddenInput()}
