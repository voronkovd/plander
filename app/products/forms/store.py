# -*- coding: utf-8 -*-
from django import forms
from django.conf import settings as sett
from django.core.files.images import get_image_dimensions
from django.utils.datastructures import MergeDict, MultiValueDict
from django_select2.forms import ModelSelect2TagWidget, ModelSelect2Widget

from products.models import GoodStore, GoodTag, GoodBrand


class TagWidget(ModelSelect2TagWidget):
    model = GoodTag
    search_fields = ['name__icontains', 'pk__startswith']
    queryset = GoodTag.objects.all()

    def render(self, name, value, attrs=None, choices=()):
        output = super(TagWidget, self).render(name, value, attrs=attrs, choices=choices)
        self.set_to_cache()
        return output

    def get_search_fields(self):
        return self.search_fields

    def value_from_datadict(self, data, files, name):
        if isinstance(data, (MultiValueDict, MergeDict)):
            values = data.getlist(name)
            pks = []
            tags = []
            for value in values:
                try:
                    value = int(value)
                    pks.append(value)
                except ValueError:
                    tags.append(value)

            if tags:
                for tag in tags:
                    try:
                        old = GoodTag.objects.get(name=tag)
                        pks.append(old.pk)
                    except GoodTag.DoesNotExist:
                        new = GoodTag.objects.create(name=tag)
                        pks.append(new.pk)
            return pks
        else:
            return data.get(name, None)


class ImageAbstract(forms.ModelForm):
    class Meta:
        abstract = True

    # def clean_image(self):
    #     image = self.cleaned_data.get("image")
    #     width = sett.IMAGE_FULL_WIDTH
    #     height = sett.IMAGE_FULL_HEIGHT
    #     dimension = round(float(width) / float(height), 1)
    #     if image:
    #         w, h = get_image_dimensions(image)
    #         if w < width and h < height:
    #             raise forms.ValidationError(
    #                 "Минимальное разрешение изображения должно быть %ix%i" % (
    #                     width, height))
    #         new_dimension = round(float(float(w) / float(h)), 1)
    #         if dimension != new_dimension:
    #             raise forms.ValidationError(
    #                 "Не верное разрешение изображения")
    #         return image


class StoreForm(ImageAbstract):
    brand = forms.ModelChoiceField(
        queryset=GoodBrand.objects.all().order_by('name'),
        label=u'Бренд', required=False,
        widget=ModelSelect2Widget(attrs={'id': 'id_store_brand'},
                                  search_fields=['name__icontains'])
    )
    slug = forms.SlugField(
        widget=forms.TextInput(attrs={'disabled': 'disabled'}), label=u'URI',
        required=False)

    class Meta:
        model = GoodStore
        fields = ['good', 'tag', 'color', 'size', 'volume', 'weight',
                  'image', 'price_real', 'price_discount', 'price',
                  'is_available', 'is_active', 'is_hot', 'comment', 'slug']
        widgets = {
            'good': forms.Select(attrs={'id': 'id_store_good'}),
            'tag': TagWidget(),
            'comment': forms.Textarea(attrs={'id': 'id_markdown_editor'}),
            'color': forms.Select(attrs={'disabled': 'disabled'}),
            'size': forms.Select(attrs={'disabled': 'disabled'}),
            'volume': forms.Select(attrs={'disabled': 'disabled'}),
            'weight': forms.Select(attrs={'disabled': 'disabled'}),
            'slug': forms.TextInput(attrs={'disabled': 'disabled'}),
        }
