# -*- coding: utf-8 -*-
from django import forms
from django_select2.forms import ModelSelect2Widget

from products.models import GoodBase, GoodGroup


class BaseForm(forms.ModelForm):
    """
    Форма базового товара
    """
    group = forms.ModelChoiceField(
        queryset=GoodGroup.objects.filter(
            parent__isnull=False).select_related(
            'parent').all().order_by('parent__name', 'name'),
        label=u'Группа', required=True,
        widget=forms.Select(attrs={'id': 'id_base_group'})
    )

    class Meta:
        model = GoodBase
        fields = [
            'brand', 'group', 'name', 'description', 'is_color', 'is_size',
            'is_volume', 'is_weight'
        ]
        widgets = {
            'description': forms.Textarea(attrs={'id': 'id_markdown_editor'}),
            'brand': ModelSelect2Widget(attrs={'id': 'id_base_brand'},
                                        search_fields=['name__icontains']),
        }
