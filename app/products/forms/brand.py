# -*- coding: utf-8 -*-
from django import forms

from products.models import GoodBrand, GoodGroup


class BrandForm(forms.ModelForm):
    """
    Форма бренда
    """
    group = forms.ModelMultipleChoiceField(
        queryset=GoodGroup.objects.filter(
            parent__isnull=False).select_related(
            'parent').all().order_by('parent__name', 'name'),
        label=u'Родительская группа', required=False)

    class Meta:
        model = GoodBrand
        fields = ['group', 'name', 'description', 'image']
        widgets = {
            'description': forms.Textarea(
                attrs={'id': 'id_markdown_editor'})
        }
