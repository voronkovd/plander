# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations

import app.utils


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='GoodBase',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
                ('description', models.TextField(verbose_name=u'Описание')),
                ('is_color', models.BooleanField(
                    default=False, verbose_name=u'Цвет')),
                ('is_size', models.BooleanField(
                    default=False, verbose_name=u'Размер')),
                ('is_volume', models.BooleanField(
                    default=False, verbose_name=u'Объем')),
                ('is_weight', models.BooleanField(
                    default=False, verbose_name=u'Вес')),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_base',
                'verbose_name': u'Базовый Товар',
                'verbose_name_plural': 'Базовые Товары',
                'permissions': (('can_change_products',
                                 u'Управление номенклатурой'),
                                ('can_change_users',
                                 u'Управление пользователями'),
                                ('can_change_orders',
                                 u'Управление заказами')),
            },
        ),
        migrations.CreateModel(
            name='GoodBrand',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
                ('slug', models.SlugField(
                    max_length=255, blank=True, unique=True,
                    verbose_name=u'URI',
                    help_text=u'Если оставить пустым, транслит наименования')
                 ),
                ('description', models.TextField(verbose_name=u'Описание')),
                ('image', models.ImageField(
                    upload_to=app.utils.upload_to, null=True, blank=True,
                    verbose_name=u'Изображение')),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_brand',
                'verbose_name': u'Бренд',
                'verbose_name_plural': u'Бренды',
            },
        ),
        migrations.CreateModel(
            name='GoodColor',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_color',
                'verbose_name': u'Вариант цвета',
                'verbose_name_plural': u'Варианты цвета',
            },
        ),
        migrations.CreateModel(
            name='GoodGallery',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('image', models.ImageField(
                    help_text=u'Минимальный размер изображения 334x222',
                    upload_to=app.utils.upload_to,
                    verbose_name=u'Изображение')),
            ],
            options={
                'ordering': ['id'],
                'default_permissions': (),
                'db_table': 'goods_gallery',
                'verbose_name': u'Изображения товара',
                'verbose_name_plural': u'Изображение товара',
            },
        ),
        migrations.CreateModel(
            name='GoodGroup',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
                ('slug', models.SlugField(
                    max_length=255, blank=True, unique=True,
                    verbose_name=u'URI',
                    help_text=u'Если оставить пустым, транслит наименования')
                 ),
                ('parent', models.ForeignKey(
                    verbose_name=u'Родительская группа', blank=True,
                    to='products.GoodGroup', null=True)),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_group',
                'verbose_name': u'Группа',
                'verbose_name_plural': u'Группы',
            },
        ),
        migrations.CreateModel(
            name='GoodSize',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_size',
                'verbose_name': u'Вариант размера',
                'verbose_name_plural': u'Варианты размеров',
            },
        ),
        migrations.CreateModel(
            name='GoodStore',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('slug', models.SlugField(
                    max_length=255, blank=True, unique=True,
                    verbose_name=u'URI',
                    help_text=u'Если оставить пустым, транслит наименования')
                 ),
                ('image', models.ImageField(
                    help_text=u'Минимальный размер изображения 334x222',
                    upload_to=app.utils.upload_to,
                    verbose_name=u'Изображение')),
                ('comment', models.TextField(verbose_name=u'Комментарий',
                                             blank=True)),
                ('price_real', models.DecimalField(
                    verbose_name=u'Реальная цена', max_digits=8,
                    decimal_places=2)),
                ('price_discount', models.DecimalField(
                    null=True, verbose_name='Цена со скидкой',
                    max_digits=8, decimal_places=2, blank=True)),
                ('price', models.DecimalField(
                    verbose_name=u'Цена', max_digits=8, decimal_places=2)),
                ('is_available', models.BooleanField(
                    default=True, verbose_name=u'Продавать')),
                ('is_active', models.BooleanField(
                    default=True, verbose_name=u'Отображать')),
                ('color', models.ForeignKey(
                    verbose_name=u'Цвет', blank=True, to='products.GoodColor',
                    null=True)),
                ('good', models.ForeignKey(
                    verbose_name=u'Базовый товар', to='products.GoodBase')),
                ('size', models.ForeignKey(
                    verbose_name=u'Размер', blank=True,
                    to='products.GoodSize', null=True)),
            ],
            options={
                'ordering': ['price'],
                'default_permissions': (),
                'db_table': 'goods_store',
                'verbose_name': u'Товар',
                'verbose_name_plural': u'Товары',
            },
        ),
        migrations.CreateModel(
            name='GoodTag',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_tag',
                'verbose_name': u'Тэг',
                'verbose_name_plural': u'Тэги',
            },
        ),
        migrations.CreateModel(
            name='GoodVolume',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_volume',
                'verbose_name': u'Вариант объема',
                'verbose_name_plural': u'Варианты объема',
            },
        ),
        migrations.CreateModel(
            name='GoodWeight',
            fields=[
                ('id', models.AutoField(
                    verbose_name='ID', serialize=False, auto_created=True,
                    primary_key=True)),
                ('name', models.CharField(
                    unique=True, max_length=255,
                    verbose_name=u'Наименование')),
            ],
            options={
                'default_permissions': (),
                'db_table': 'goods_weight',
                'verbose_name': u'Вариант веса',
                'verbose_name_plural': u'Варианты веса',
            },
        ),
        migrations.AddField(
            model_name='goodstore',
            name='tag',
            field=models.ManyToManyField(
                to='products.GoodTag', verbose_name=u'Тэги', blank=True),
        ),
        migrations.AddField(
            model_name='goodstore',
            name='volume',
            field=models.ForeignKey(
                verbose_name=u'Объем', blank=True, to='products.GoodVolume',
                null=True),
        ),
        migrations.AddField(
            model_name='goodstore',
            name='weight',
            field=models.ForeignKey(verbose_name=u'Вес', blank=True,
                                    to='products.GoodWeight', null=True),
        ),
        migrations.AddField(
            model_name='goodgallery',
            name='store',
            field=models.ForeignKey(
                verbose_name=u'Товар', to='products.GoodStore'),
        ),
        migrations.AddField(
            model_name='goodbrand',
            name='group',
            field=models.ManyToManyField(
                to='products.GoodGroup', verbose_name=u'Группы товаров'),
        ),
        migrations.AddField(
            model_name='goodbase',
            name='brand',
            field=models.ForeignKey(
                verbose_name=u'Бренд', to='products.GoodBrand'),
        ),
        migrations.AddField(
            model_name='goodbase',
            name='group',
            field=models.ForeignKey(
                verbose_name=u'Группа', to='products.GoodGroup'),
        ),
    ]
