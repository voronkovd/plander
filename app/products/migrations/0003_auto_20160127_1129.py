# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20160121_1224'),
    ]

    operations = [
        migrations.AlterField(
            model_name='goodstore',
            name='price_discount',
            field=models.DecimalField(decimal_places=2, max_digits=8, blank=True, help_text='\u0417\u0430\u0447\u0435\u0440\u043a\u043d\u0443\u0442\u0430\u044f \u0446\u0435\u043d\u0430', null=True, verbose_name='\u0421\u0442\u0430\u0440\u0430\u044f \u0446\u0435\u043d\u0430'),
        ),
        migrations.AlterField(
            model_name='goodstore',
            name='price_real',
            field=models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430', max_digits=8, decimal_places=2),
        ),
    ]
