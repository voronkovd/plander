# -*- coding: utf-8 -*-
from django import template

from products.models import GoodGroup, GoodStore

register = template.Library()


@register.inclusion_tag('public/tags/group.html')
def group_links():
    """
    Возвщаем шаблон со списком ссылок на категории
    :return: template
    """
    groups = GoodGroup.objects.filter(
        parent__isnull=True).all().order_by('name')
    return {'groups': groups}


@register.filter('open_menu')
def open_menu(request, menu):
    """
    По текущему урлу определяем в каком блоке меню находимся
    и возвращаем css класс на раскрытие блока
    :param request:
    :param menu: str
    :return: str
    """
    url = request.resolver_match.url_name
    properties = [
        'admin_good_group_index', 'admin_good_group_create',
        'admin_good_group_update', 'admin_good_group_delete',
        'admin_good_brand_index', 'admin_good_brand_create',
        'admin_good_brand_update', 'admin_good_brand_delete',
        'admin_good_tag_index', 'admin_good_tag_create',
        'admin_good_tag_update', 'admin_good_tag_delete'
    ]
    attributes = [
        'admin_good_color_index', 'admin_good_color_create',
        'admin_good_color_update', 'admin_good_color_delete',
        'admin_good_size_index', 'admin_good_size_create',
        'admin_good_size_update', 'admin_good_size_delete',
        'admin_good_volume_index', 'admin_good_volume_create',
        'admin_good_volume_update', 'admin_good_volume_delete',
        'admin_good_weight_index', 'admin_good_weight_create',
        'admin_good_weight_update', 'admin_good_weight_delete'
    ]
    goods = [
        'admin_good_base_index', 'admin_good_base_create',
        'admin_good_base_update', 'admin_good_base_delete',
        'admin_good_store_index', 'admin_good_store_create',
        'admin_good_store_update', 'admin_good_store_delete',
        'admin_good_base_detail', 'admin_good_store_detail'
    ]
    users = [
        'admin_user_user_index', 'admin_user_user_update',
        'admin_user_user_delete', 'admin_user_group_index',
        'admin_user_group_create', 'admin_user_group_update',
        'admin_user_group_delete'
    ]
    orders = [
        'admin_order_index', 'admin_order_archive', 'admin_order_detail'
    ]
    if menu == 'goods':
        if url in goods:
            return 'in'
    if menu == 'properties':
        if url in properties:
            return 'in'
    elif menu == 'attributes':
        if url in attributes:
            return 'in'
    elif menu == 'users':
        if url in users:
            return 'in'
    elif menu == 'orders':
        if url in orders:
            return 'in'
    return ''


@register.inclusion_tag('public/tags/hot_goods.html')
def get_hot_goods():
    """
    Возвщаем шаблон со списком популярных товаров
    :return: template
    """
    hot_stores = GoodStore.objects.filter(
        is_hot=True, is_active=True, is_available=True
    ).all().select_related('good').order_by('-id')
    return {'hot_stores': hot_stores}
