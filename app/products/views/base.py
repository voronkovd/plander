# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views import generic
from django_filters.views import FilterView

from admin.views.index import JsonResponseMixin
from products.filters.base import BaseFilter
from products.forms.base import BaseForm
from products.models.base import GoodBase
from users.views.permission import PermissionRequiredMixin


class Create(PermissionRequiredMixin, generic.CreateView):
    """
    Добавление базового товара
    """
    form_class = BaseForm
    model = GoodBase
    permission_required = 'products.can_change_products'
    success_url = '/admin/products/bases/'
    template_name = 'admin/bases/create.html'

    def get_success_url(self):
        return reverse('admin_good_base_detail',
                       kwargs={'pk': self.object.pk})


class Delete(PermissionRequiredMixin, generic.DeleteView):
    """
    Удаление базового товара
    """
    model = GoodBase
    permission_required = 'products.can_change_products'
    success_url = '/admin/products/bases/'
    template_name = 'admin/confirm_delete.html'


class Detail(PermissionRequiredMixin, generic.DetailView):
    """
    Просмотр базового товара
    """
    model = GoodBase
    permission_required = 'products.can_change_products'
    queryset = GoodBase.objects.select_related('group', 'brand')
    template_name = 'admin/bases/view.html'


class DetailAjax(PermissionRequiredMixin, JsonResponseMixin,
                 generic.DetailView):
    """
    Просмотр базового товара Json
    """
    model = GoodBase
    queryset = GoodBase.objects.values('is_color', 'is_size', 'is_volume',
                                       'is_weight')
    permission_required = 'products.can_change_products'


class Index(PermissionRequiredMixin, FilterView):
    """
    Список базовых товаров
    """
    filterset_class = BaseFilter
    model = GoodBase
    paginate_by = 50
    permission_required = 'products.can_change_products'
    queryset = GoodBase.objects.select_related('group', 'brand')
    template_name = 'admin/bases/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    """
    Редактирование базового товара
    """
    form_class = BaseForm
    model = GoodBase
    permission_required = 'products.can_change_products'
    template_name = 'admin/bases/update.html'

    def get_success_url(self):
        return reverse('admin_good_base_detail',
                       kwargs={'pk': self.object.pk})


create_view = Create.as_view()
delete_view = Delete.as_view()
detail_view = Detail.as_view()
detail_ajax = DetailAjax.as_view()
index_view = Index.as_view()
update_view = Update.as_view()
