# -*- coding: utf-8 -*-
from django.views import generic
from django_filters.views import FilterView

from products.filters.group import GroupFilter
from products.forms.group import GroupForm
from products.models.group import GoodGroup
from users.views.permission import PermissionRequiredMixin

SUCCESS_URL = '/admin/products/groups/'


class Create(PermissionRequiredMixin, generic.CreateView):
    form_class = GroupForm
    model = GoodGroup
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/groups/create.html'


class Delete(PermissionRequiredMixin, generic.DeleteView):
    model = GoodGroup
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/confirm_delete.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = GroupFilter
    model = GoodGroup
    paginate_by = 50
    permission_required = 'products.can_change_products'
    queryset = GoodGroup.objects.select_related('parent')
    template_name = 'admin/groups/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    form_class = GroupForm
    model = GoodGroup
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/groups/update.html'


index_view = Index.as_view()
create_view = Create.as_view()
update_view = Update.as_view()
delete_view = Delete.as_view()
