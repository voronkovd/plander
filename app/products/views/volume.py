# -*- coding: utf-8 -*-
from django.views import generic
from django_filters.views import FilterView

from products.filters.volume import VolumeFilter
from products.models.volume import GoodVolume
from users.views.permission import PermissionRequiredMixin

SUCCESS_URL = '/admin/products/volumes/'


class Create(PermissionRequiredMixin, generic.CreateView):
    fields = ['name']
    model = GoodVolume
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/volumes/create.html'


class Delete(PermissionRequiredMixin, generic.DeleteView):
    model = GoodVolume
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/confirm_delete.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = VolumeFilter
    model = GoodVolume
    paginate_by = 50
    permission_required = 'products.can_change_products'
    template_name = 'admin/volumes/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    fields = ['name']
    model = GoodVolume
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/volumes/update.html'


index_view = Index.as_view()
create_view = Create.as_view()
update_view = Update.as_view()
delete_view = Delete.as_view()
