# -*- coding: utf-8 -*-
from django.views import generic
from django_filters.views import FilterView

from products.forms.brand import BrandForm
from products.filters.brand import BrandFilter
from products.models.brand import GoodBrand
from users.views.permission import PermissionRequiredMixin

SUCCESS_URL = '/admin/products/brands/'


class Create(PermissionRequiredMixin, generic.CreateView):
    """

    """
    form_class = BrandForm
    model = GoodBrand
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/brands/create.html'


class Good(PermissionRequiredMixin, generic.DetailView):
    """

    """
    model = GoodBrand
    permission_required = 'products.can_change_products'
    template_name = 'admin/brands/_goods.html'


class GroupSelect(PermissionRequiredMixin, generic.DetailView):
    """

    """
    model = GoodBrand
    permission_required = 'products.can_change_products'
    template_name = 'admin/groups/_brand_select.html'


class Delete(PermissionRequiredMixin, generic.DeleteView):
    """

    """
    model = GoodBrand
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/confirm_delete.html'


class Index(PermissionRequiredMixin, FilterView):
    """

    """
    filterset_class = BrandFilter
    model = GoodBrand
    paginate_by = 50
    permission_required = 'products.can_change_products'
    queryset = GoodBrand.objects.prefetch_related('group')
    template_name = 'admin/brands/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    """

    """
    form_class = BrandForm
    model = GoodBrand
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/brands/update.html'


create_view = Create.as_view()
good_select = Good.as_view()
group_select = GroupSelect.as_view()
index_view = Index.as_view()
update_view = Update.as_view()
delete_view = Delete.as_view()
