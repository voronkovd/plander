# -*- coding: utf-8 -*-
from django.views import generic
from django_filters.views import FilterView

from products.filters.weight import WeightFilter
from products.models.weight import GoodWeight
from users.views.permission import PermissionRequiredMixin

SUCCESS_URL = '/admin/products/weights/'


class Create(PermissionRequiredMixin, generic.CreateView):
    fields = ['name']
    model = GoodWeight
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/weights/create.html'


class Delete(PermissionRequiredMixin, generic.DeleteView):
    model = GoodWeight
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/confirm_delete.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = WeightFilter
    model = GoodWeight
    paginate_by = 50
    permission_required = 'products.can_change_products'
    template_name = 'admin/weights/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    fields = ['name']
    model = GoodWeight
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/weights/update.html'


index_view = Index.as_view()
create_view = Create.as_view()
update_view = Update.as_view()
delete_view = Delete.as_view()
