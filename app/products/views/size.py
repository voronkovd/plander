# -*- coding: utf-8 -*-
from django.views import generic
from django_filters.views import FilterView

from products.filters.size import SizeFilter
from products.models.size import GoodSize
from users.views.permission import PermissionRequiredMixin

SUCCESS_URL = '/admin/products/sizes/'


class Create(PermissionRequiredMixin, generic.CreateView):
    fields = ['name']
    model = GoodSize
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/sizes/create.html'


class Delete(PermissionRequiredMixin, generic.DeleteView):
    model = GoodSize
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/confirm_delete.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = SizeFilter
    model = GoodSize
    paginate_by = 50
    permission_required = 'products.can_change_products'
    template_name = 'admin/sizes/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    fields = ['name']
    model = GoodSize
    permission_required = 'products.can_change_products'
    success_url = SUCCESS_URL
    template_name = 'admin/sizes/update.html'


index_view = Index.as_view()
create_view = Create.as_view()
update_view = Update.as_view()
delete_view = Delete.as_view()
