# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.views import generic
from django_filters.views import FilterView
from easy_thumbnails.files import get_thumbnailer
from products.forms.store import StoreForm
from products.forms.gallery import GalleryForm
from products.filters.store import StoreFilter
from products.models.gallery import GoodGallery
from products.models.store import GoodStore
from users.views.permission import PermissionRequiredMixin


class Create(PermissionRequiredMixin, generic.CreateView):
    form_class = StoreForm
    model = GoodStore
    permission_required = 'products.can_change_products'
    template_name = 'admin/stores/create.html'

    def get_success_url(self):
        return reverse('admin_good_store_detail',
                       kwargs={'pk': self.object.pk})

    def get_initial(self):
        if 'pk' in self.request.GET:
            try:
                pk = int(self.request.GET.get('pk'))
                store = GoodStore.objects.get(pk=pk)
                return {
                    'brand': store.good.brand, 'good': store.good,
                    'tag': store.tag.all, 'color': store.color,
                    'size': store.size, 'volume': store.volume,
                    'weight': store.weight, 'price_real': store.price_real,
                    'price_discount': store.price_discount,
                    'price': store.price, 'is_available': store.is_available,
                    'is_active': store.is_active, 'comment': store.comment
                }
            except GoodStore.DoesNotExist:
                pass
            except ValueError:
                pass
        return {}


class Delete(PermissionRequiredMixin, generic.DeleteView):
    model = GoodStore
    permission_required = 'products.can_change_products'
    success_url = '/admin/products/stores/'
    template_name = 'admin/confirm_delete.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = StoreFilter
    model = GoodStore
    paginate_by = 50
    permission_required = 'products.can_change_products'
    queryset = GoodStore.objects.select_related(
        'good', 'color', 'size', 'volume', 'weight')
    template_name = 'admin/stores/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    form_class = StoreForm
    model = GoodStore
    permission_required = 'products.can_change_products'
    template_name = 'admin/stores/update.html'

    def get_success_url(self):
        return reverse('admin_good_store_detail',
                       kwargs={'pk': self.object.pk})

    def get_initial(self):
        return {'brand': self.object.good.brand}


class Detail(PermissionRequiredMixin, generic.DetailView):
    model = GoodStore
    form_class = GalleryForm
    permission_required = 'products.can_change_products'
    template_name = 'admin/stores/view.html'

    def get_context_data(self, **kwargs):
        context = super(Detail, self).get_context_data(**kwargs)
        context['form'] = GalleryForm(initial={"store": self.object})
        return context

    def post(self, request, *args, **kwargs):
        form = GalleryForm(request.POST, request.FILES)
        if form.is_valid():
            image_object = form.save()
            options = {'size': (250, 164), 'crop': True}
            get_thumbnailer(image_object.image).get_thumbnail(options)
            self.object = self.get_object()
            context = super(Detail, self).get_context_data(**kwargs)
            context['form'] = form
            return self.render_to_response(context=context)

        else:
            self.object = self.get_object()
            context = super(Detail, self).get_context_data(**kwargs)
            context['form'] = form
            return self.render_to_response(context=context)


class DeleteGallery(generic.DeleteView):
    model = GoodGallery
    permission_required = 'products.can_change_products'
    template_name = 'admin/confirm_delete.html'

    def get_success_url(self):
        return reverse('admin_good_store_detail',
                       kwargs={'pk': self.object.store_id})


index_view = Index.as_view()
detail_view = Detail.as_view()
create_view = Create.as_view()
update_view = Update.as_view()
delete_view = Delete.as_view()
delete_gallery = DeleteGallery.as_view()
