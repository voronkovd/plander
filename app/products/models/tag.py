# -*- coding: utf-8 -*-
from django_cache_manager.cache_manager import CacheManager

from products.models.attribute import Attribute


class GoodTag(Attribute):
    """
    Тэги
    """
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_tag'
        default_permissions = ()
        verbose_name = u'Тэг'
        verbose_name_plural = u'Тэги'
