# -*- coding: utf-8 -*-
from django.db import models
from django_cache_manager.cache_manager import CacheManager

from products.models.attribute import Attribute
from products.models.brand import GoodBrand
from products.models.group import GoodGroup


class GoodBase(Attribute):
    """
    Базовый товар, относиться к группе товаров и брендов

    """
    brand = models.ForeignKey(GoodBrand, verbose_name=u'Бренд')
    group = models.ForeignKey(GoodGroup, verbose_name=u'Группа')
    description = models.TextField(verbose_name=u'Описание')
    is_color = models.BooleanField(verbose_name=u'Цвет', default=False)
    is_size = models.BooleanField(verbose_name=u'Размер', default=False)
    is_volume = models.BooleanField(verbose_name=u'Объем', default=False)
    is_weight = models.BooleanField(verbose_name=u'Вес', default=False)

    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_base'
        verbose_name = u'Базовый Товар'
        verbose_name_plural = u'Базовые Товары'
        default_permissions = ()
        permissions = (
            ('can_change_products', u'Управление номенклатурой'),
            ('can_change_users', u'Управление пользователями'),
            ('can_change_orders', u'Управление заказами')
        )

    def get_brand(self):
        """
        Возвращаем наименование бренда
        :return: str
        """
        return unicode(self.brand.name)

    def get_group(self):
        """
        Возвращаем наименование группы
        :return: str
        """
        return unicode(self.group.name)

    def get_stores(self):
        """
        Возвращаем список товаров базового товара
        :return: list objects
        """
        return self.goodstore_set.all().order_by('price_real')


