# -*- coding: utf-8 -*-
from slugify import slugify

from django.db import models


class Attribute(models.Model):
    """
    Абстрактный класс
    Почти у каждой модели есть уникальное наименование
    """
    name = models.CharField(verbose_name=u'Наименование', max_length=255,
                            unique=True)

    class Meta:
        """
        Указываем что класс абстрактный
        Сортировка по умолчанию по наименованию
        """
        abstract = True
        ordering = ['name']

    def __unicode__(self):
        """
        Возвращаем строку при запросе объекта
        :return: str
        """
        return unicode(self.name)


class Slug(models.Model):
    """
    Абстрактный класс
    Добавляем slug (для url)
    """
    slug = models.SlugField(
        verbose_name=u'URI', max_length=255, unique=True, blank=True,
        help_text=u'Если оставить пустым, транслит наименования')

    class Meta:
        """
        Указываем что класс абстрактный
        """
        abstract = True

    def set_slug(self):
        """
        Задаем URI
        """
        self.slug = (slugify(self, lower=True, spaces=False,
                             only_ascii=True)).replace("'", "")
