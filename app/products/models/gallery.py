# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django_cache_manager.cache_manager import CacheManager

from app.utils import upload_to as to
from products.models.store import GoodStore


class GoodGallery(models.Model):
    """
    Галерея изображений к товару
    """
    store = models.ForeignKey(GoodStore, verbose_name=u'Товар')
    image = models.ImageField(upload_to=to, verbose_name=u'Изображение',
                              help_text=settings.IMAGE_HELP_TEXT)
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_gallery'
        default_permissions = ()
        ordering = ['id']
        verbose_name = u'Изображения товара'
        verbose_name_plural = u'Изображение товара'
