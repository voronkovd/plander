# -*- coding: utf-8 -*-
from django_cache_manager.cache_manager import CacheManager

from products.models.attribute import Attribute


class GoodVolume(Attribute):
    """
    Атрибут товара объем
    """
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_volume'
        default_permissions = ()
        verbose_name = u'Вариант объема'
        verbose_name_plural = u'Варианты объема'
