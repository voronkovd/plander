# -*- coding: utf-8 -*-
import random
import string
from django.conf import settings
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django_cache_manager.cache_manager import CacheManager
from slugify import slugify
from app.utils import upload_to as to, generate_hash
from products.models.attribute import Slug
from products.models.base import GoodBase
from products.models.color import GoodColor
from products.models.size import GoodSize
from products.models.tag import GoodTag
from products.models.volume import GoodVolume
from products.models.weight import GoodWeight


class GoodStore(Slug):
    """
    Товар собранный из основного товара и атрибутов
    """
    good = models.ForeignKey(GoodBase, verbose_name=u'Базовый товар')
    tag = models.ManyToManyField(GoodTag, verbose_name=u'Тэги', blank=True)
    color = models.ForeignKey(GoodColor, verbose_name=u'Цвет', null=True,
                              blank=True)
    size = models.ForeignKey(GoodSize, verbose_name=u'Размер', null=True,
                             blank=True)
    volume = models.ForeignKey(GoodVolume, verbose_name=u'Объем', null=True,
                               blank=True)
    weight = models.ForeignKey(GoodWeight, verbose_name=u'Вес', null=True,
                               blank=True)
    image = models.ImageField(
        upload_to=to, verbose_name=u'Изображение', null=True, blank=True,
        help_text=settings.IMAGE_HELP_TEXT)
    comment = models.TextField(verbose_name=u'Комментарий', blank=True)
    price_real = models.DecimalField(max_digits=8, decimal_places=2,
                                     verbose_name=u'Цена магазина')
    price_discount = models.DecimalField(
        max_digits=8, decimal_places=2, null=True, blank=True,
        verbose_name=u'Старая цена', help_text=u'Зачеркнутая цена')
    price = models.DecimalField(max_digits=8, decimal_places=2,
                                verbose_name=u'Цена')
    is_available = models.BooleanField(verbose_name=u'Продавать',
                                       default=True)
    is_active = models.BooleanField(verbose_name=u'Отображать',
                                    default=True)
    is_hot = models.BooleanField(verbose_name=u'Популярный', default=False)

    objects = CacheManager()

    def __unicode__(self):
        """
        Возвращаем строку при запросе объекта
        :return: str
        """
        return unicode(self.get_name())

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_store'
        default_permissions = ()
        ordering = ['price']
        verbose_name = u'Товар'
        verbose_name_plural = u'Товары'

    def random_word(self, length):
        """
        Генерируем строку по заданной длине и возвращаем
        :param length: длина строки
        :return: str
        """
        return ''.join(random.choice(string.lowercase) for i in range(length))

    def set_slug(self):
        """
        Задаем URI
        """
        if not self.slug:
            self.slug = (slugify(self.generate_slug, lower=True, spaces=False,
                                 only_ascii=True)).replace("'", "")

    @property
    def generate_slug(self):
        """
        Генерируем uri
        :return: str
        """
        return self.get_name() + unicode(self.random_word(3))

    def get_base(self):
        """
        Возвращаем наименование базового товара
        :return: str
        """
        return unicode(self.good.name)

    def check_attribute(self):
        if self.color:
            return True
        if self.size:
            return True
        if self.volume:
            return True
        if self.weight:
            return True
        return False

    def get_color(self):
        """
        Возвращаем наименование цвета
        :return: str
        """
        if self.color:
            return unicode(self.color.name)
        else:
            return ''

    def get_size(self):
        """
        Возвращаем наименование размера
        :return: str
        """
        if self.size:
            return unicode(self.size.name)
        else:
            return ''

    def get_volume(self):
        """
        Возвращаем наименование объема
        :return: str
        """
        if self.volume:
            return unicode(self.volume.name)
        else:
            return ''

    def get_weight(self):
        """
        Возвращаем наименование веса
        :return: str
        """
        if self.weight:
            return unicode(self.weight.name)
        else:
            return ''

    def get_name(self):
        """
        Возвращаем Наименование с учетом атрибутов
        :return: str
        """
        suffix = ''
        if self.get_color() != '':
            suffix += ' - ' + self.get_color()
        elif self.get_size() != '':
            suffix += ' - ' + self.get_size()
        elif self.get_volume() != '':
            suffix += ' - ' + self.get_volume()
        elif self.get_weight() != '':
            suffix += ' - ' + self.get_weight()
        if suffix != '':
            return unicode("%s %s" % (self.good.name, suffix))
        return unicode(self.good.name)

    def get_gallery(self):
        """
        Возвращаем список изображений галереи
        :return: list objects
        """
        return self.goodgallery_set.all()

    def get_price(self):
        return unicode(int(self.price))

    def get_price_real(self):
        return unicode(int(self.price_real))

    def get_price_discount(self):
        return unicode(int(self.price_discount))

    def get_hash(self):
        return generate_hash(self.pk)


@receiver(pre_save, sender=GoodStore)
def create_slug(instance, **kwargs):
    """
    Отлавливаем сигнал перед сохранением и вызываем self.set_slug()
    :param instance:
    :param kwargs:
    :return:
    """
    instance.set_slug()
