# -*- coding: utf-8 -*-
from django_cache_manager.cache_manager import CacheManager

from products.models.attribute import Attribute


class GoodSize(Attribute):
    """
    Атрибут товара размер
    """
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_size'
        default_permissions = ()
        verbose_name = u'Вариант размера'
        verbose_name_plural = u'Варианты размеров'
