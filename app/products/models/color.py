# -*- coding: utf-8 -*-
from django_cache_manager.cache_manager import CacheManager

from products.models.attribute import Attribute


class GoodColor(Attribute):
    """
    Атрибут товара цвет
    """
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_color'
        default_permissions = ()
        verbose_name = u'Вариант цвета'
        verbose_name_plural = u'Варианты цвета'
