# -*- coding: utf-8 -*-
from django_cache_manager.cache_manager import CacheManager

from products.models.attribute import Attribute


class GoodWeight(Attribute):
    """
    Атрибут товара вес
    """
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_weight'
        default_permissions = ()
        verbose_name = u'Вариант веса'
        verbose_name_plural = u'Варианты веса'
