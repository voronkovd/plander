# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django_cache_manager.cache_manager import CacheManager

from app.utils import upload_to as to
from products.models.attribute import Attribute, Slug
from products.models.group import GoodGroup


class GoodBrand(Attribute, Slug):
    """
    Бренды, связь с группой товаров, многие ко многим
    дл удобства при добавлении товаров
    """
    group = models.ManyToManyField(GoodGroup, verbose_name=u'Группы товаров')
    description = models.TextField(verbose_name=u'Описание')
    image = models.ImageField(upload_to=to, verbose_name=u'Изображение',
                              null=True, blank=True)
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_brand'
        default_permissions = ()
        verbose_name = u'Бренд'
        verbose_name_plural = u'Бренды'

    def get_groups(self):
        """
        Возвращаем список групп бренда
        :return: list objects
        """
        return self.group.filter(
            parent__isnull=False
        ).select_related('parent').all().order_by('parent__name', 'name')

    def get_goods(self):
        """
        Возвращаем спискок базовых товаров
        :return: list objects
        """
        return self.goodbase_set.all().order_by('name')

@receiver(pre_save, sender=GoodBrand)
def create_slug(instance, **kwargs):
    """
    Отлавливаем сигнал перед сохранением и вызываем self.set_slug()
    :param instance:
    :param kwargs:
    :return:
    """
    instance.set_slug()
