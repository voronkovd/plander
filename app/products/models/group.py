# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django_cache_manager.cache_manager import CacheManager

from products.models.attribute import Attribute, Slug


class GoodGroup(Attribute, Slug):
    """
    Группы товаров, с бесконечной вложенностью
    """
    parent = models.ForeignKey('self', verbose_name=u'Родительская группа',
                               null=True, blank=True)
    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'goods_group'
        default_permissions = ()
        verbose_name = u'Группа'
        verbose_name_plural = u'Группы'

    def get_parent_name(self):
        """
        Возвращаем наименование родительской группы
        :return: str
        """
        name = u' --- '
        if self.parent:
            name = self.parent.name
        return name

    def get_count_product(self):
        """
        Возвращаем колличество товара в группе
        :return: int
        """
        count = 0
        if self.goodbase_set:
            count = self.goodbase_set.count()
        return count

    def get_slug(self):
        """
        Возвращаем uri
        :return: str
        """
        return unicode(self.slug)

    def get_children(self):
        """
        Возвращаем список дочерних групп
        :return: list objects
        """
        return self.goodgroup_set.all().order_by('name')

    def __unicode__(self):
        if self.parent:
            return unicode("%s -> %s" % (self.parent.name, self.name))
        else:
            return unicode("%s" % self.name)

    def get_brands(self):
        """
        Возвращаем список брендов
        :return: list objects
        """
        return self.goodbrand_set.all().order_by('name')


@receiver(pre_save, sender=GoodGroup)
def create_slug(instance, **kwargs):
    """
    Отлавливаем сигнал перед сохранением и вызываем self.set_slug()
    :param instance:
    :param kwargs:
    :return:
    """
    instance.set_slug()
