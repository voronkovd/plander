# -*- coding: utf-8 -*-
import ujson

from django import http
from django.views import generic

from products import models as product
from orders import models as order
from users import models as user
from users.views.permission import PermissionRequiredMixin


class JsonResponseMixin(object):
    """
    Mixin для конвертацию результатав json
    """
    def render_to_response(self, context):
        context = self.convert_context_to_json(context)
        return http.HttpResponse(context, content_type='application/json')

    def convert_context_to_json(self, context):
        return ujson.dumps(context)


class Index(PermissionRequiredMixin, generic.TemplateView):
    """
    Главная страница админки
    """
    permission_required = [
        'products.can_change_products',
        'products.can_change_users',
        'products.can_change_orders',
    ]
    template_name = 'admin/index.html'

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        properties_count = []
        attributes_count = []
        product_count = []
        user_count = []
        order_count = []
        group_count = product.GoodGroup.objects.count()
        properties_count.append(
            {'label': u'Группы', 'value': group_count}
        )
        brand_count = product.GoodBrand.objects.count()
        properties_count.append(
            {'label': u'Бренды', 'value': brand_count}
        )
        tag_count = product.GoodTag.objects.count()
        properties_count.append(
            {'label': u'Тэги', 'value': tag_count}
        )
        color_count = product.GoodColor.objects.count()
        attributes_count.append(
            {'label': u'Варианты цвета', 'value': color_count}
        )
        size_count = product.GoodSize.objects.count()
        attributes_count.append(
            {'label': u'Варианты размера', 'value': size_count}
        )
        volume_count = product.GoodVolume.objects.count()
        attributes_count.append(
            {'label': u'Варианты объема', 'value': volume_count}
        )
        weight_count = product.GoodWeight.objects.count()
        attributes_count.append(
            {'label': u'Варианты веса', 'value': weight_count}
        )
        base_count = product.GoodBase.objects.count()
        product_count.append(
            {'label': u'Базовые', 'value': base_count}
        )
        all_store_count = product.GoodStore.objects.count()
        product_count.append(
            {'label': u'Все', 'value': all_store_count}
        )
        is_available_store_count = product.GoodStore.objects.filter(
            is_available=True).count()
        product_count.append(
            {'label': u'В продаже', 'value': is_available_store_count}
        )
        is_active_store_count = product.GoodStore.objects.filter(
            is_available=True).count()
        product_count.append(
            {'label': u'Отображаемые', 'value': is_active_store_count}
        )
        all_user_count = user.User.objects.count()
        user_count.append(
            {'label': u'Все', 'value': all_user_count}
        )
        admin_user_count = user.User.objects.filter(is_superuser=True).count()
        user_count.append(
            {'label': u'Администраторы', 'value': admin_user_count}
        )
        simple_user_count = user.User.objects.filter(
            is_superuser=False, groups__isnull=True).count()
        user_count.append(
            {'label': u'Покупатели', 'value': simple_user_count}
        )
        active_user_count = user.User.objects.filter(is_active=True).count()
        user_count.append(
            {'label': u'Подтвержденные',
             'value': active_user_count}
        )

        all_order_count = order.Order.objects.count()
        order_count.append(
            {'label': u'Все',
             'value': all_order_count}
        )
        new_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_NEW).count()
        order_count.append(
            {'label': u'Новые',
             'value': new_order_count}
        )
        work_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_WORK).count()
        order_count.append(
            {'label': u'В работе',
             'value': work_order_count}
        )
        change_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_CHANGE).count()
        order_count.append(
            {'label': u'Ожидают подтверждения',
             'value': change_order_count}
        )
        delivery_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_DELIVERY).count()
        order_count.append(
            {'label': u'В пути',
             'value': delivery_order_count}
        )
        arrived_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_ARRIVED).count()
        order_count.append(
            {'label': u'Доставлены',
             'value': arrived_order_count}
        )
        success_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_SUCCESS).count()
        order_count.append(
            {'label': u'Выполнены',
             'value': success_order_count}
        )
        aborted_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_ABORTED).count()
        order_count.append(
            {'label': u'Отменены',
             'value': aborted_order_count}
        )
        return_order_count = order.Order.objects.filter(
            status=order.Order.STATUS_RETURN).count()
        order_count.append(
            {'label': u'Возврат',
             'value': return_order_count}
        )
        context['properties_count'] = properties_count
        context['attributes_count'] = attributes_count
        context['product_count'] = product_count
        context['user_count'] = user_count
        context['order_count'] = order_count
        return context


index_view = Index.as_view()
