# -*- coding: utf-8 -*-
"""
Здесь храним урлы отображений
"""
from django.conf.urls import url
from django.conf.urls import patterns
from django.conf.urls import include
from admin.views import index

urlpatterns = patterns('',
                       url(r'^$', index.index_view, name='admin_index'),
                       (r'^products/', include('products.urls')),
                       (r'^users/', include('users.urls')),
                       (r'^orders/', include('orders.urls')),
                       )
