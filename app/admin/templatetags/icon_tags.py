# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter(name="get_icon")
def get_icon(value):
    """
    Возвращаем иконку в зависимости от значения
    :param value: Bool
    :return: html
    """
    if value:
        return '<i class="glyphicon glyphicon-ok-circle"></i>'
    else:
        return '<i class="glyphicon glyphicon-remove-circle"></i>'
