{% load url from future %}
{% load humanize %}
Ваш заказ прибыл в пункт выдачи.

Заказ № {{ object.get_number }} от {{ object.created|date:"d/m/Y" }} - https://{{ site.domain }}{% url 'public_order_detail' object.pk %}

С уважением, Администрация сайта {{ site.name }}