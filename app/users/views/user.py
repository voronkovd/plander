# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.views import generic
from django_filters.views import FilterView

from users.forms import UserProfileForm, PasswordChangeForm
from users.filters import UserFilter
from users.models import User
from users.views.permission import PermissionRequiredMixin, LoginRequiredMixin


class ChangePassword(LoginRequiredMixin, generic.FormView):
    form_class = PasswordChangeForm
    success_url = '/admin/users/profile'
    template_name = 'admin/users/change_password.html'

    def get_form_kwargs(self):
        kwargs = super(ChangePassword, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs


class Delete(PermissionRequiredMixin, generic.DeleteView):
    model = User
    permission_required = 'products.can_change_users'
    success_url = '/admin/users/'
    template_name = 'admin/confirm_delete.html'


class Detail(generic.DetailView):
    model = User
    permission_required = 'products.can_change_users'
    template_name = 'admin/users/detail.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = UserFilter
    model = User
    paginate_by = 50
    permission_required = 'products.can_change_users'
    template_name = 'admin/users/index.html'


class Profile(LoginRequiredMixin, generic.TemplateView):
    template_name = 'admin/users/profile.html'

    def get_context_data(self, **kwargs):
        context = super(Profile, self).get_context_data(**kwargs)
        context['user'] = get_object_or_404(User,
                                            pk=self.request.user.pk)
        return context


class Update(PermissionRequiredMixin, generic.UpdateView):
    model = User
    form_class = UserProfileForm
    permission_required = 'products.can_change_users'
    success_url = '/admin/users/'
    template_name = 'admin/users/update.html'


change_password_view = ChangePassword.as_view()
delete_view = Delete.as_view()
detail_view = Detail.as_view()
index_view = Index.as_view()
profile_view = Profile.as_view()
update_view = Update.as_view()
