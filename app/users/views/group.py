# -*- coding: utf-8 -*-
from django.views import generic
from django.contrib.auth.models import Group
from django_filters.views import FilterView

from users.forms import GroupForm
from users.filters import GroupFilter
from users.views.permission import PermissionRequiredMixin

SUCCESS_URL = '/admin/users/groups/'


class Create(PermissionRequiredMixin, generic.CreateView):
    form_class = GroupForm
    model = Group
    permission_required = 'products.can_change_users'
    success_url = SUCCESS_URL
    template_name = 'admin/user_groups/create.html'


class Delete(PermissionRequiredMixin, generic.DeleteView):
    model = Group
    permission_required = 'products.can_change_users'
    success_url = SUCCESS_URL
    template_name = 'admin/confirm_delete.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = GroupFilter
    model = Group
    paginate_by = 50
    permission_required = 'products.can_change_users'
    queryset = Group.objects.prefetch_related('permissions')
    template_name = 'admin/user_groups/index.html'


class Update(PermissionRequiredMixin, generic.UpdateView):
    form_class = GroupForm
    model = Group
    permission_required = 'products.can_change_users'
    success_url = SUCCESS_URL
    template_name = 'admin/user_groups/update.html'


create_view = Create.as_view()
delete_view = Delete.as_view()
index_view = Index.as_view()
update_view = Update.as_view()
