# -*- coding: utf-8 -*-
from django.contrib.auth.models import Group
from django.forms.widgets import TextInput
import django_filters

from users.models import User


class GroupFilter(django_filters.FilterSet):
    """
    Фильтр групп пользователей, поиск по наименованиям
    """
    name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Наименование'}))

    class Meta:
        model = Group
        fields = ['name']


class UserFilter(django_filters.FilterSet):
    """
    Фильтр пользователей, поиск по фамилии
    """
    last_name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Фамилия'}))
    first_name = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Имя'}))
    username = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Телефон'}))
    email = django_filters.CharFilter(
        lookup_type='icontains',
        widget=TextInput({'placeholder': u'Email'}))

    class Meta:
        model = User
        fields = ['last_name', 'first_name', 'username', 'email']
