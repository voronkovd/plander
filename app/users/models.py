# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractUser, Permission
from django.core.mail import send_mail


class User(AbstractUser):
    class Meta:
        default_permissions = ()
        unique_together = 'email',

    def __unicode__(self):
        return unicode(self.get_full_name())

    def get_full_name(self):
        full_name = '%s %s' % (self.last_name, self.first_name)
        return unicode(full_name.strip())

    def get_short_name(self):
        return unicode(self.first_name)

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)


class Perm(Permission):
    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        proxy = True
