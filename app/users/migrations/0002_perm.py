# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import migrations
import django.contrib.auth.models


class Migration(migrations.Migration):
    dependencies = [
        ('auth', '0006_require_contenttypes_0002'), ('users', '0001_initial')
    ]

    operations = [
        migrations.CreateModel(
            name='Perm',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('auth.permission',),
            managers=[
                (b'objects', django.contrib.auth.models.PermissionManager()),
            ],
        ),
    ]
