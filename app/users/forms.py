# -*- coding: utf-8 -*-
from django import forms
from django.core import validators
from registration.forms import RegistrationForm
from registration.users import UserModel
from django_select2.forms import ModelSelect2MultipleWidget
from django.contrib.auth.models import Group
from users.models import Perm

User = UserModel()


class CustomRegistrationForm(RegistrationForm):
    username = forms.CharField(
        label=u'Номер телефона', help_text=u'Например: 89991234567',
        validators=[validators.RegexValidator(r'^([8])?([79]\d{9})$',
                                              u'Неверный номер телефона',
                                              'invalid')],
        error_messages={
            'unique': u'Пользователь с таким номера телефона уже существует'})
    first_name = forms.CharField(label=u'Имя')
    last_name = forms.CharField(label=u'Фамилия')
    email = forms.EmailField(
        label=u'Электронная почта',
        error_messages={
            'unique': (u'Пользователь с такой электронной почтой'
                       u' уже существует')})

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2')


class UserProfileForm(forms.ModelForm):
    username = forms.CharField(
        label=u'Номер телефона', help_text=u'Например: 89991234567',
        validators=[validators.RegexValidator(r'^([8])?([79]\d{9})$',
                                              u'Неверный номер телефона',
                                              'invalid')],
        error_messages={
            'unique': u'Пользователь с таким номера телефона уже существует'})

    class Meta:
        model = User
        fields = ['last_name', 'first_name', 'username', 'email', 'groups',
                  'is_superuser', 'is_active']
        widgets = {
            'groups': ModelSelect2MultipleWidget(
                search_fields=['name__icontains'])
        }


class GroupForm(forms.ModelForm):
    permissions = forms.ModelMultipleChoiceField(
        queryset=Perm.objects.filter(
            content_type__app_label='products',
            content_type__model='goodbase').select_related(
            'content_type').all().order_by('name'), label=u'Права доступа',
        widget=ModelSelect2MultipleWidget(search_fields=['name__icontains'])
    )

    class Meta:
        model = Group
        fields = ['name', 'permissions']


class PasswordChangeForm(forms.Form):
    old_password = forms.CharField(label=u'Старый пароль',
                                   widget=forms.PasswordInput)
    password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'Еще раз', widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_old_password(self):
        old_password = self.cleaned_data["old_password"]
        if not self.user.check_password(old_password):
            raise forms.ValidationError(u'Не верный старый пароль')
        return old_password

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(u'Пароли не совпадают')
        return password2

    def save(self, commit=True):
        self.user.set_password(self.cleaned_data["password1"])
        if commit:
            self.user.save()
        return self.user

    def _get_changed_data(self):
        data = super(PasswordChangeForm, self).changed_data
        for name in self.fields.keys():
            if name not in data:
                return []
        return ['password']
    changed_data = property(_get_changed_data)
