# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns
from views import user, group

urlpatterns = patterns('',

                       url(r'^$',
                           user.index_view, name='admin_user_user_index'),
                       url(r'^update/(?P<pk>[\w-]+)$',
                           user.update_view, name='admin_user_user_update'),
                       url(r'^delete/(?P<pk>[\w-]+)$',
                           user.delete_view, name='admin_user_user_delete'),
                       url(r'^view/(?P<pk>[\w-]+)$',
                           user.detail_view, name='admin_user_user_detail'),
                       url(r'^groups/$',
                           group.index_view, name='admin_user_group_index'),
                       url(r'^groups/create/$',
                           group.create_view, name='admin_user_group_create'),
                       url(r'^groups/update/(?P<pk>[\w-]+)$',
                           group.update_view, name='admin_user_group_update'),
                       url(r'^groups/delete/(?P<pk>[\w-]+)$',
                           group.delete_view, name='admin_user_group_delete'),
                       url(r'^profile/$',
                           user.profile_view, name='admin_user_profile'),
                       url(r'^change/password$', user.change_password_view,
                           name='admin_user_change_password'),
                       )
