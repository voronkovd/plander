# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Basket',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.PositiveSmallIntegerField(default=1, verbose_name='\u041a\u043e\u043b\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('price', models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430', max_digits=8, decimal_places=2)),
                ('added', models.DateField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('store', models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='products.GoodStore')),
                ('user', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'orders_basket',
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440 \u0432 \u043a\u043e\u0440\u0437\u0438\u043d\u0435',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b \u0432 \u043a\u043e\u0440\u0437\u0438\u043d\u0435',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.PositiveIntegerField(default=0, verbose_name='\u0422\u0435\u043a\u0443\u0449\u0438\u0439 \u0441\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u041d\u043e\u0432\u044b\u0439'), (1, '\u0412 \u0440\u0430\u0431\u043e\u0442\u0435'), (2, '\u0412 \u043f\u0443\u0442\u0438'), (3, '\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d'), (4, '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d'), (5, '\u041e\u0442\u043c\u0435\u043d\u0435\u043d'), (6, '\u0412\u043e\u0437\u0432\u0440\u0430\u0442')])),
                ('comment', models.TextField(null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('created', models.DateField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0438 \u0432\u0440\u0435\u043c\u044f \u0434\u043e\u0431\u0430\u0432\u043b\u0435\u043d\u0438\u044f')),
                ('user', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'orders_orders',
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b',
            },
        ),
        migrations.CreateModel(
            name='OrderStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.PositiveIntegerField(default=0, verbose_name='\u0422\u0435\u043a\u0443\u0449\u0438\u0439 \u0441\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u041d\u043e\u0432\u044b\u0439'), (1, '\u0412 \u0440\u0430\u0431\u043e\u0442\u0435'), (2, '\u0412 \u043f\u0443\u0442\u0438'), (3, '\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d'), (4, '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d'), (5, '\u041e\u0442\u043c\u0435\u043d\u0435\u043d'), (6, '\u0412\u043e\u0437\u0432\u0440\u0430\u0442')])),
                ('comment', models.TextField(null=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
                ('changed', models.DateField(auto_now_add=True, verbose_name='\u0418\u0437\u043c\u0435\u043d\u0435\u043d')),
                ('order', models.ForeignKey(verbose_name='\u0417\u0430\u043a\u0430\u0437', to='orders.Order')),
                ('user', models.ForeignKey(verbose_name='\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'orders_statuses',
                'verbose_name': '\u0417\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b',
            },
        ),
        migrations.CreateModel(
            name='OrderStore',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('count', models.PositiveSmallIntegerField(default=1, verbose_name='\u041a\u043e\u043b\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e')),
                ('price', models.DecimalField(verbose_name='\u0426\u0435\u043d\u0430', max_digits=8, decimal_places=2)),
                ('order', models.ForeignKey(verbose_name='\u0417\u0430\u043a\u0430\u0437', to='orders.Order')),
                ('store', models.ForeignKey(verbose_name='\u0422\u043e\u0432\u0430\u0440', to='products.GoodStore')),
            ],
            options={
                'db_table': 'orders_stores',
                'verbose_name': '\u0422\u043e\u0432\u0430\u0440 \u0432 \u0437\u0430\u043a\u0430\u0437\u0435',
                'verbose_name_plural': '\u0422\u043e\u0432\u0430\u0440\u044b \u0432 \u0437\u0430\u043a\u0430\u0437\u0435',
            },
        ),
    ]
