# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0002_auto_20160127_1129'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='order',
            options={'ordering': ['status', '-id'], 'verbose_name': '\u0417\u0430\u043a\u0430\u0437', 'verbose_name_plural': '\u0417\u0430\u043a\u0430\u0437\u044b'},
        ),
        migrations.AddField(
            model_name='basket',
            name='price_real',
            field=models.DecimalField(default=0.0, verbose_name='\u0426\u0435\u043d\u0430 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430', max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='orderstore',
            name='price_real',
            field=models.DecimalField(default=0.0, verbose_name='\u0426\u0435\u043d\u0430 \u043c\u0430\u0433\u0430\u0437\u0438\u043d\u0430', max_digits=8, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0422\u0435\u043a\u0443\u0449\u0438\u0439 \u0441\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u041d\u043e\u0432\u044b\u0439'), (1, '\u0412 \u0440\u0430\u0431\u043e\u0442\u0435'), (2, '\u041e\u0436\u0438\u0434\u0430\u0435\u0442 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u044f'), (3, '\u0412 \u043f\u0443\u0442\u0438'), (4, '\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d'), (5, '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d'), (6, '\u041e\u0442\u043c\u0435\u043d\u0435\u043d'), (7, '\u0412\u043e\u0437\u0432\u0440\u0430\u0442')]),
        ),
        migrations.AlterField(
            model_name='orderstatus',
            name='status',
            field=models.PositiveIntegerField(default=0, verbose_name='\u0422\u0435\u043a\u0443\u0449\u0438\u0439 \u0441\u0442\u0430\u0442\u0443\u0441', choices=[(0, '\u041d\u043e\u0432\u044b\u0439'), (1, '\u0412 \u0440\u0430\u0431\u043e\u0442\u0435'), (2, '\u041e\u0436\u0438\u0434\u0430\u0435\u0442 \u043f\u043e\u0434\u0442\u0432\u0435\u0440\u0436\u0434\u0435\u043d\u0438\u044f'), (3, '\u0412 \u043f\u0443\u0442\u0438'), (4, '\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d'), (5, '\u0412\u044b\u043f\u043e\u043b\u043d\u0435\u043d'), (6, '\u041e\u0442\u043c\u0435\u043d\u0435\u043d'), (7, '\u0412\u043e\u0437\u0432\u0440\u0430\u0442')]),
        ),
    ]
