# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views import generic
from django_filters.views import FilterView

from orders.filters.order import OrderFilter, ArchiveFilter
from orders.models import Order, OrderStatus
from users.views.permission import PermissionRequiredMixin


class Archive(PermissionRequiredMixin, FilterView):
    filterset_class = ArchiveFilter
    model = Order
    paginate_by = 50
    permission_required = 'products.can_change_orders'
    queryset = Order.objects.filter(
        status__in=Order.get_complete_statuses()).select_related('user')
    template_name = 'admin/orders/index.html'

    def get_context_data(self, **kwargs):
        context = super(Archive, self).get_context_data(**kwargs)
        context['archive'] = True
        return context


class Detail(PermissionRequiredMixin, generic.DeleteView):
    model = Order
    permission_required = 'products.can_change_orders'
    queryset = Order.objects.select_related('user')
    template_name = 'admin/orders/detail.html'


class Index(PermissionRequiredMixin, FilterView):
    filterset_class = OrderFilter
    model = Order
    paginate_by = 50
    permission_required = 'products.can_change_orders'
    queryset = Order.objects.filter(
        status__in=Order.get_active_statuses()).select_related('user')
    template_name = 'admin/orders/index.html'


class Update(PermissionRequiredMixin, generic.DetailView):
    model = Order
    permission_required = 'products.can_change_orders'

    def get(self, request, *args, **kwargs):
        order = self.get_object()
        url = reverse('admin_order_detail', kwargs={'pk': order.pk})
        status = int(request.GET.get('status'))
        if status in Order.get_all_statuses():
            order.status = status
            order.save()
            OrderStatus.objects.create(
                user=request.user, order=order, status=status)
        return HttpResponseRedirect(url)


archive_view = Archive.as_view()
detail_view = Detail.as_view()
index_view = Index.as_view()
update_view = Update.as_view()

