# -*- coding: utf-8 -*-
from django.conf.urls import url
from django.conf.urls import patterns

from orders.views import order, store

urlpatterns = patterns('',
                       url(r'^$',
                           order.index_view, name='admin_order_index'),
                       url(r'^archive/$',
                           order.archive_view, name='admin_order_archive'),
                       url(r'^view/(?P<pk>[\w-]+)$',
                           order.detail_view, name='admin_order_detail'),
                       url(r'^update/(?P<pk>[\w-]+)$',
                           order.update_view, name='admin_order_update'),
                       )
