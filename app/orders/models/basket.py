# -*- coding: utf-8 -*-
from django.db import models
from django_cache_manager.cache_manager import CacheManager

from products.models import GoodStore
from users.models import User


class Basket(models.Model):
    """
    Корзина товаров пользователя
    """
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    store = models.ForeignKey(GoodStore, verbose_name=u'Товар')
    count = models.PositiveSmallIntegerField(default=1,
                                             verbose_name=u'Колличество')
    price_real = models.DecimalField(max_digits=8, decimal_places=2,
                                     verbose_name=u'Цена магазина',
                                     default=0.00)
    price = models.DecimalField(max_digits=8, decimal_places=2,
                                verbose_name=u'Цена')
    added = models.DateField(auto_now_add=True, editable=False,
                             verbose_name=u'Дата и время добавления')

    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'orders_basket'
        verbose_name = u'Товар в корзине'
        verbose_name_plural = u'Товары в корзине'

    def get_price(self):
        return unicode(int(self.price))
