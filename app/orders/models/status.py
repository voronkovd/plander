# -*- coding: utf-8 -*-
from django.db import models
from django_cache_manager.cache_manager import CacheManager

from orders.models import Order
from users.models import User


class OrderStatus(models.Model):
    """
    Список статусов заказа
    """
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    order = models.ForeignKey(Order, verbose_name=u'Заказ')
    status = models.PositiveIntegerField(
        choices=Order.STATUSES, default=Order.STATUS_NEW,
        verbose_name=u'Текущий статус')
    comment = models.TextField(verbose_name=u'Комментарий', null=True,
                               blank=True)
    changed = models.DateField(auto_now_add=True, editable=False,
                               verbose_name=u'Изменен')

    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'orders_statuses'
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'

    def get_status(self):
        return dict(Order.STATUSES).get(self.status)
