# -*- coding: utf-8 -*-
from smtplib import SMTPAuthenticationError

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django_cache_manager.cache_manager import CacheManager
from django.template import Context
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives

from orders.models import Basket
from users.models import User


class Order(models.Model):
    """
    Список заказов
    """
    STATUS_NEW = 0
    STATUS_WORK = 1
    STATUS_CHANGE = 2
    STATUS_DELIVERY = 3
    STATUS_ARRIVED = 4
    STATUS_SUCCESS = 5
    STATUS_ABORTED = 6
    STATUS_RETURN = 7
    NEW_ORDER_EMAIL_THEME = u'Создан новый заказ'
    ACTIVE_STATUSES = (
        (STATUS_NEW, u'Новый'),
        (STATUS_WORK, u'В работе'),
        (STATUS_CHANGE, u'Ожидает подтверждения'),
        (STATUS_DELIVERY, u'В пути'),
        (STATUS_ARRIVED, u'Доставлен'),
    )
    ARCHIVE_STATUSES = (
        (STATUS_ARRIVED, u'Доставлен'),
        (STATUS_SUCCESS, u'Выполнен'),
        (STATUS_ABORTED, u'Отменен'),
        (STATUS_RETURN, u'Возврат')
    )
    STATUSES = (
        (STATUS_NEW, u'Новый'),
        (STATUS_WORK, u'В работе'),
        (STATUS_CHANGE, u'Ожидает подтверждения'),
        (STATUS_DELIVERY, u'В пути'),
        (STATUS_ARRIVED, u'Доставлен'),
        (STATUS_SUCCESS, u'Выполнен'),
        (STATUS_ABORTED, u'Отменен'),
        (STATUS_RETURN, u'Возврат'),
    )
    EMAIL_STATUSES = {
        STATUS_NEW: {
            'public': {
                'title': u'Новый Заказ на plunder.kz',
                'template_txt': 'public/mail/new_order.txt',
                'template_html': 'public/mail/new_order.html',
            },
            'admin': {
                'title': u'Новый заказ сайте plunder.kz',
                'template_txt': 'admin/mail/new_order.txt',
                'template_html': 'admin/mail/new_order.html',
            }
        },
        STATUS_CHANGE: {
            'public': {
                'title': u'Ваш заказ на plunder.kz был изменен',
                'template_txt': 'public/mail/change_order.txt',
                'template_html': 'public/mail/change_order.html',
            },
        },
        STATUS_ARRIVED: {
            'public': {
                'title': u'Ваш заказ на plunder.kz доставлен',
                'template_txt': 'public/mail/arrived_order.txt',
                'template_html': 'public/mail/arrived_order.html',
            },
        },
        STATUS_ABORTED: {
            'public': {
                'title': u'Ваш заказ на plunder.kz отменен',
                'template_txt': 'public/mail/aborted_order.txt',
                'template_html': 'public/mail/aborted_order.html',
            },
        }
    }
    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    status = models.PositiveIntegerField(
        choices=STATUSES, default=STATUS_NEW,
        verbose_name=u'Текущий статус')
    comment = models.TextField(verbose_name=u'Комментарий', null=True,
                               blank=True)
    created = models.DateField(auto_now_add=True, editable=False,
                               verbose_name=u'Дата и время добавления')

    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'orders_orders'
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'
        ordering = ['status', '-id']

    def __unicode__(self):
        return unicode(self.pk)

    def get_all_sum(self):
        all_sum = 0
        store = self.orderstore_set.all().order_by('-id')
        if store:
            for good in store:
                all_sum += good.count * good.price
        return all_sum

    def get_number(self):
        max_length = 11
        current_length = len(self.__unicode__())
        str_length = max_length - current_length
        if max_length <= current_length:
            return self
        i = 0
        return_str = ''
        while i < str_length:
            return_str += '0'
            i += 1
        return return_str + self.__unicode__()

    def get_status(self):
        return dict(Order.STATUSES).get(self.status)

    @staticmethod
    def get_active_statuses():
        return [
            Order.STATUS_NEW, Order.STATUS_WORK, Order.STATUS_CHANGE,
            Order.STATUS_DELIVERY, Order.STATUS_ARRIVED
        ]

    @staticmethod
    def get_complete_statuses():
        return [
            Order.STATUS_SUCCESS, Order.STATUS_ABORTED, Order.STATUS_RETURN
        ]

    @staticmethod
    def get_all_statuses():
        return [
            Order.STATUS_NEW, Order.STATUS_WORK, Order.STATUS_CHANGE,
            Order.STATUS_DELIVERY, Order.STATUS_ARRIVED, Order.STATUS_SUCCESS,
            Order.STATUS_ABORTED, Order.STATUS_RETURN
        ]

    def is_new(self):
        if self.status == Order.STATUS_NEW:
            return True
        return False

    def is_work(self):
        if self.status == Order.STATUS_WORK:
            return True
        return False

    def is_change(self):
        if self.status == Order.STATUS_CHANGE:
            return True
        return False

    def is_delivery(self):
        if self.status == Order.STATUS_DELIVERY:
            return True
        return False

    def is_arrived(self):
        if self.status == Order.STATUS_ARRIVED:
            return True
        return False

    def is_success(self):
        if self.status == Order.STATUS_SUCCESS:
            return True
        return False


def from_basket_to_order(instance):
    basket = Basket.objects.filter(user=instance.user)
    for item in basket:
        instance.orderstore_set.create(
            order=instance, store=item.store, count=item.count,
            price=item.price, price_real=item.price_real)
        item.delete()
    return basket


def clear_basket(instance):
    instance.all().delete()


def send_email(send_from, send_to, title, tmp_txt, tmp_html, context):
    try:
        c = Context(context)
        text_content = render_to_string(tmp_txt, c)
        html_content = render_to_string(tmp_html, c)
        email = EmailMultiAlternatives(title, text_content, send_from)
        email.attach_alternative(html_content, "text/html")
        if isinstance(send_to, list):
            for to in send_to:
                email.to.append(to)
        else:
            email.to = [send_to]
        email.send()
    except SMTPAuthenticationError:
        pass


def send_email_user(instance):
    order = Order.objects.get(pk=instance.pk)
    if order.status in Order.EMAIL_STATUSES:
        send_from = settings.DEFAULT_FROM_EMAIL
        send_to = instance.user.email
        title = Order.EMAIL_STATUSES[order.status]['public']['title']
        t_txt = Order.EMAIL_STATUSES[order.status]['public']['template_txt']
        t_html = Order.EMAIL_STATUSES[order.status]['public']['template_html']
        context = {'object': order, 'all_sum': order.get_all_sum()}
        send_email(send_from, send_to, title, t_txt, t_html, context)


def send_email_managers(instance):
    order = Order.objects.get(pk=instance.pk)
    users = User.objects.filter(
        models.Q(
            groups__permissions__name='can_change_orders') | models.Q(
            is_superuser=True)).all()
    send_to = []
    if users:
        for user in users:
            send_to.append(user.email)
    if order.status == Order.STATUS_NEW and send_to:
        send_from = settings.DEFAULT_FROM_EMAIL
        title = Order.EMAIL_STATUSES[order.status]['admin']['title']
        t_txt = Order.EMAIL_STATUSES[order.status]['admin']['template_txt']
        t_html = Order.EMAIL_STATUSES[order.status]['admin']['template_html']
        context = {'object': order, 'all_sum': order.get_all_sum()}
        send_email(send_from, send_to, title, t_txt, t_html, context)


@receiver(post_save, sender=Order)
def after_order_save(instance, **kwargs):
    basket = from_basket_to_order(instance)
    clear_basket(basket)
    send_email_user(instance)
    send_email_managers(instance)
