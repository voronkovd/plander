# -*- coding: utf-8 -*-
from django.db import models
from django_cache_manager.cache_manager import CacheManager

from orders.models import Order
from products.models import GoodStore


class OrderStore(models.Model):
    """
    Список товаров заказа
    """
    store = models.ForeignKey(GoodStore, verbose_name=u'Товар')
    order = models.ForeignKey(Order, verbose_name=u'Заказ')
    count = models.PositiveSmallIntegerField(default=1,
                                             verbose_name=u'Колличество')
    price_real = models.DecimalField(max_digits=8, decimal_places=2,
                                     verbose_name=u'Цена магазина',
                                     default=0.00)
    price = models.DecimalField(max_digits=8, decimal_places=2,
                                verbose_name=u'Цена')

    objects = CacheManager()

    class Meta:
        """
        Указываем таблицу
        """
        db_table = 'orders_stores'
        verbose_name = u'Товар в заказе'
        verbose_name_plural = u'Товары в заказе'

    def get_price(self):
        return unicode(int(self.price))

    def get_price_real(self):
        return unicode(int(self.price_real))