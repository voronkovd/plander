# -*- coding: utf-8 -*-
from django import template

from orders.models import Basket

register = template.Library()


@register.inclusion_tag('public/orders/_basket.html')
def get_basket(user):
    """
    Возвщаем шаблон со списком популярных товаров
    :return: template
    """
    all_sum = 0
    basket = None
    if user.is_authenticated():
        basket = Basket.objects.filter(user=user).all().order_by('-id')
        if basket:
            for good in basket:
                all_sum += good.count * good.price

    return {'basket': basket, 'all_sum': int(all_sum)}
