# -*- coding: utf-8 -*-
import django_filters
from datetimewidget.widgets import DateWidget
from django.forms.widgets import TextInput

from orders.models import Order


class OrderFilter(django_filters.FilterSet):
    id = django_filters.CharFilter(
        lookup_type='icontains', label=u'№',
        widget=TextInput({'placeholder': u'№'}))
    status = django_filters.ChoiceFilter(
        choices=((None, '---------'),) + Order.ACTIVE_STATUSES,
        required=False)
    created = django_filters.DateFilter(
        input_formats=('%Y-%m-%d',),
        widget=DateWidget(bootstrap_version=3,
                          options={'language': 'ru', 'format': 'yyyy-mm-dd'}))

    class Meta:
        model = Order
        fields = ['id', 'status', 'user', 'created']


class ArchiveFilter(OrderFilter):
    status = django_filters.ChoiceFilter(
        choices=((None, '---------'),) + Order.ARCHIVE_STATUSES,
        required=False)
